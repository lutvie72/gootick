package id.igit.gootick.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.timessquare.CalendarPickerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.util.EventRxBus;
import id.igit.gootick.helper.GeneraleHelper;

public class CalendarActivity extends AppCompatActivity implements
        View.OnClickListener,
        CalendarPickerView.OnDateSelectedListener {

    @BindView(R.id.calendar_view)
    CalendarPickerView calendar;
    @BindView(R.id.start_date)
    TextView textViewStartDate;
    @BindView(R.id.end_date)
    TextView textViewEndDate;
    @BindView(R.id.button_save)
    Button buttonSave;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        ButterKnife.bind(this);
        buttonSave.setOnClickListener(this);

        ImageView toolbarClose = toolbar.findViewById(R.id.toolbar_close);
        toolbarClose.setOnClickListener(this);

        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);

        Date today = new Date();
        calendar.init(today, nextYear.getTime()).inMode(CalendarPickerView.SelectionMode.SINGLE);
        calendar.setOnDateSelectedListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.toolbar_close) {
            onBackPressed();
        }

        if (view.getId() == R.id.button_save) {
            List<Date> dateList = new ArrayList<>();
            if (!calendar.getSelectedDates().isEmpty()) {
                dateList.add(calendar.getSelectedDates().get(0));
                if (getIntent().getStringExtra("mode").equals("end")){
                    calendar.getSelectedDates().add(calendar.getSelectedDates().get(0));
                    dateList.add(calendar.getSelectedDates().get(0));
                }

                EventRxBus.getInstance().pushEvent(dateList);
                finish();
            }
        }
    }

    @Override
    public void onDateSelected(Date date) {
        if (calendar.getSelectedDates().size() == 1) {
            textViewStartDate.setText(GeneraleHelper.convertDate(date, GeneraleHelper.SHORT_TEXT_DATE_FORMAT));
            textViewEndDate.setText(getString(R.string.end_date));
        } else {
            textViewEndDate.setText(GeneraleHelper.convertDate(date, GeneraleHelper.SHORT_TEXT_DATE_FORMAT));
        }
    }

    @Override
    public void onDateUnselected(Date date) {

    }
}
