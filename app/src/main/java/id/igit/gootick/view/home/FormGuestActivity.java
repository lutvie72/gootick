package id.igit.gootick.view.home;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.util.EventRxBus;

public class FormGuestActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.layout_infant)
    RelativeLayout layoutInfant;
    @BindView(R.id.imageview_adults_positif)
    ImageView imageViewAdultPositif;
    @BindView(R.id.imageview_adults_minus)
    ImageView imageViewAdultMinus;
    @BindView(R.id.imageview_children_positif)
    ImageView imageViewChildrenPositif;
    @BindView(R.id.imageview_children_minus)
    ImageView imageViewChildrenMinus;
    @BindView(R.id.imageview_infants_positif)
    ImageView imageViewInfantsPositif;
    @BindView(R.id.imageview_infants_minus)
    ImageView imageViewInfantsMinus;
    @BindView(R.id.textview_adults)
    TextView textViewAdults;
    @BindView(R.id.textview_children)
    TextView textViewChildren;
    @BindView(R.id.textview_infants)
    TextView textViewInfants;
    @BindView(R.id.button_save)
    Button buttonSave;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private int max = 7;
    private int minAdult = 1;
    private int minChild = 0;
    private int minInfant = 0;
    private int adults = 1;
    private int children = 0;
    private int infants = 0;

    private static int PLUS = 1;
    private static int MINUS = 0;

    private int mode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_guest);
        ButterKnife.bind(this);

        ImageView toolbarClose = toolbar.findViewById(R.id.toolbar_close);
        toolbarClose.setOnClickListener(this);

        imageViewAdultMinus.setOnClickListener(this);
        imageViewAdultPositif.setOnClickListener(this);
        imageViewChildrenMinus.setOnClickListener(this);
        imageViewChildrenPositif.setOnClickListener(this);
        imageViewInfantsMinus.setOnClickListener(this);
        imageViewInfantsPositif.setOnClickListener(this);
        buttonSave.setOnClickListener(this);

        mode = getIntent().getIntExtra("mode", 0);
        if (mode == 1) {
            layoutInfant.setVisibility(View.GONE);
            adults = 1;
            minAdult = 1;
            max = 4;
        }else {
            max = 7;
        }

        textViewAdults.setText(adults+"");
    }

    private int count(int value, int action, int min) {
        if (action == MINUS) {
            if (value == min) {
                return min;
            }

            return value - 1;
        } else if (action == PLUS) {
            if (value == max) {
                return max;
            }
            return value + 1;
        }

        return value;
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.toolbar_close) {
            onBackPressed();
        }

        if (view.getId() == R.id.imageview_adults_positif) {
            adults = count(adults, PLUS, minAdult);
            textViewAdults.setText(adults + "");
        }

        if (view.getId() == R.id.imageview_adults_minus) {
            adults = count(adults, MINUS, minAdult);
            textViewAdults.setText(adults + "");
        }

        if (view.getId() == R.id.imageview_children_positif) {
            children = count(children, PLUS, minChild);
            textViewChildren.setText(children + "");
        }

        if (view.getId() == R.id.imageview_children_minus) {
            children = count(children, MINUS, minChild);
            textViewChildren.setText(children + "");
        }

        if (view.getId() == R.id.imageview_infants_positif) {
            infants = count(infants, PLUS, minInfant);
            textViewInfants.setText(infants + "");
        }

        if (view.getId() == R.id.imageview_infants_minus) {
            infants = count(infants, MINUS, minInfant);
            textViewInfants.setText(infants + "");
        }

        if (view.getId() == R.id.button_save) {
            Map<String, Integer> map = new HashMap<String, Integer>();
            map.put("adults", adults);
            map.put("children", children);
            map.put("infants", infants);
            EventRxBus.getInstance().pushEvent(map);
            finish();
        }
    }
}
