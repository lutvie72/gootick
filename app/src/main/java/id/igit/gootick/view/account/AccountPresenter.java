package id.igit.gootick.view.account;

import android.os.Handler;
import android.os.Looper;

import id.igit.gootick.entity.account.Data;
import id.igit.gootick.entity.account.User;
import id.igit.gootick.service.BaseApiService;
import id.igit.gootick.view.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Kristiawan on 07/09/18.
 */
public class AccountPresenter implements BasePresenter<Data> {

    private AccountView view;
    private BaseApiService apiService;

    public AccountPresenter(AccountView view) {
        this.view = view;
        this.apiService = BaseApiService.newInstance();
    }

    @Override
    public void find(Data param) {

    }

    @Override
    public void findById(Data param) {
        view.showProgress();
        apiService.getAccountService().getUsers(param.getPuid())
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            if (response.isSuccessful()) {
                                if (response.body().getData() != null) {
                                    view.onLoadSuccess(response.body());
                                } else {
                                    view.onLoadError(response.body().getMessage());
                                }

                            }

                            view.removeProgress();
                        });
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            view.onLoadError(t.getMessage());
                            view.removeProgress();
                        });
                    }
                });
    }
}