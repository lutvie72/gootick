package id.igit.gootick.view.home.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.igit.gootick.R;
import id.igit.gootick.entity.traindata.gootick.stations.Datum;
import id.igit.gootick.util.EventRxBus;

/**
 * Created by Kristiawan on 17/05/18.
 */
public class SearchTrainFilterAdapter extends RecyclerView.Adapter<SearchTrainFilterAdapter.ViewHolder> implements Filterable {
    private List<Datum> stations;
    private List<Datum> filteredList;
    private String type;
    private Context context;

    public SearchTrainFilterAdapter(List<Datum> items, String type) {
        stations = items;
        filteredList = items;
        this.type = type;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_search_filter, parent, false);
        if (context == null) {
            this.context = parent.getContext();
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Datum station = filteredList.get(position);
        holder.textLocation.setText(station.getCityName());
        holder.textAirport.setText(station.getStationCode() + " - " + station.getStationName());
    }

    @Override
    public int getItemCount() {
        return filteredList != null ? filteredList.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList = stations;
                } else {
                    List<Datum> filteredStations = new ArrayList<>();
                    for (Datum row : stations) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
//                        if (row.getRuang().toLowerCase().startsWith(charString.toLowerCase())) {
//                            filteredList.add(row);
//                        }

                        boolean isfirst = false;

                        for (String s : row.getCityName().toLowerCase().split(" ")) {
                            if (s.startsWith(charString.toLowerCase())) {
                                filteredStations.add(row);
                                isfirst = true;
                                break;
                            } else if (row.getCityName().toLowerCase().startsWith(charString.toLowerCase())) {
                                filteredStations.add(row);
                                isfirst = true;
                                break;
                            }
                        }

                        if (!isfirst) {
                            for (String s : row.getStationCode().concat(" ").concat(row.getStationName()).toLowerCase().split(" ")) {
                                if (s.startsWith(charString.toLowerCase())) {
                                    filteredStations.add(row);
                                    break;
                                } else if (row.getStationName().toLowerCase().startsWith(charString.toLowerCase())) {
                                    filteredStations.add(row);
                                    break;
                                }
                            }
                        }
                    }

                    filteredList = filteredStations;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<Datum>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView textLocation;
        public TextView textAirport;

        public ViewHolder(View view) {
            super(view);
            textLocation = (TextView) view.findViewById(R.id.text_location);
            textAirport = (TextView) view.findViewById(R.id.text_airport);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Datum station = filteredList.get(getAdapterPosition());
            station.setType(type);

            EventRxBus.getInstance().pushEvent(station);
            ((Activity) context).finish();
        }
    }
}
