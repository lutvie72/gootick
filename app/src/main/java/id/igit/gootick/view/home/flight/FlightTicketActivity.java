package id.igit.gootick.view.home.flight;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.App;
import id.igit.gootick.R;
import id.igit.gootick.entity.airplanedata.Result;
import id.igit.gootick.entity.airplanedata.SearchQueries;
import id.igit.gootick.entity.airplanedata.gotick.Datum;
import id.igit.gootick.entity.airplanedata.gotick.PlaneTiket;
import id.igit.gootick.helper.GeneraleHelper;
import id.igit.gootick.helper.ViewHelper;
import id.igit.gootick.util.LocalStorage;
import id.igit.gootick.view.home.adapter.FlightTicketAdapter;

public class FlightTicketActivity extends AppCompatActivity implements FlightView, View.OnClickListener {

    @BindView(R.id.recycler_ticket)
    RecyclerView recyclerView;
    @BindView(R.id.button_change_search)
    Button buttonChangeSearch;
    @BindView(R.id.cardview)
    CardView cardViewInfo;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.text_destination)
    TextView textViewDestination;
    @BindView(R.id.text_guest)
    TextView textViewGuest;
    @BindView(R.id.text_start_date)
    TextView textViewStartDate;
    @BindView(R.id.text_end_date)
    TextView textViewEndDate;

    private LinearLayoutManager linearLayoutManager;

    private SearchQueries searchQueries;
    private List<Datum> airplaneTickets;
    private LocalStorage localStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.airplane_ticket_activity);
        ButterKnife.bind(this);
        localStorage = new LocalStorage(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText(R.string.result_searching);
        textViewTitle.setTextSize(20);
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        buttonChangeSearch.setOnClickListener(this);

        searchQueries = (SearchQueries) App.getInstance().fromJSON(getIntent().getStringExtra("param"), SearchQueries.class);
        if (searchQueries.getRetDate() == null) {
            textViewEndDate.setText("-");

        } else {
            textViewEndDate.setText(GeneraleHelper.convertDate(GeneraleHelper.convertStringToDate(searchQueries.getRetDate()), GeneraleHelper.SHORT_TEXT_DATE_FORMAT, new Locale(localStorage.getLanguage().toLowerCase())));

        }

        textViewDestination.setText(searchQueries.getmFrom() + "-" + searchQueries.getmTo());
        textViewGuest.setText(searchQueries.getAdult() + " " + getString(R.string.adult) + " " + searchQueries.getChild() + " " + getString(R.string.children) + " " + searchQueries.getInfant() + " " + getString(R.string.infant));
        textViewStartDate.setText(GeneraleHelper.convertDate(GeneraleHelper.convertStringToDate(searchQueries.getDate()), GeneraleHelper.SHORT_TEXT_DATE_FORMAT, new Locale(localStorage.getLanguage().toLowerCase())));

        airplaneTickets = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(new FlightTicketAdapter(this, airplaneTickets));

        FlightPresenter presenter = new FlightPresenter(this);
        presenter.find(searchQueries);
    }

    @Override
    public void showProgress() {
        ViewHelper.getInstance().showProgressDialog(this);
    }

    @Override
    public void removeProgress() {
        ViewHelper.getInstance().dismisProgressDialog();
    }

    @Override
    public void onLoadError(String error) {
//        ViewHelper.showMessage(this, error);
        Log.d(this.getLocalClassName(), error);
        Toast.makeText(this, R.string.message_error_connection_server, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLoadSuccess(PlaneTiket result) {
        if (result.getStatus() != 200) {
            Toast.makeText(this, result.getMessage(), Toast.LENGTH_SHORT).show();
        }

        if (result.getData().getData() != null) {
            if (result.getData().getData().isEmpty()){
                Toast.makeText(this, R.string.message_ticket_not_available, Toast.LENGTH_LONG).show();
            }

            airplaneTickets.clear();
            airplaneTickets.addAll(result.getData().getData());

            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == buttonChangeSearch.getId()) {
            finish();
        }
    }
}
