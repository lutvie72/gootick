package id.igit.gootick.view.home.train;

import id.igit.gootick.entity.traindata.TrainTicket;
import id.igit.gootick.entity.traindata.gootick.TrainTiket;
import id.igit.gootick.view.BaseView;

/**
 * Created by Kristiawan on 17/05/18.
 */
public interface TrainTicketView extends BaseView<TrainTiket> {
}
