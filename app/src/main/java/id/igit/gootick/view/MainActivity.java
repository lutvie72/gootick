package id.igit.gootick.view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.Image;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.helper.ViewHelper;
import id.igit.gootick.util.LocalStorage;
import id.igit.gootick.view.account.AccountFragment;
import id.igit.gootick.view.home.HomeFragment;
import id.igit.gootick.view.notification.NotificationFragment;
import id.igit.gootick.view.order.OrderFragment;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    private TextView textViewTitle;
    private ImageView imageView;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = item -> {
        int id = item.getItemId();
        Fragment fragment = null;
        if (id == R.id.action_home) {
            fragment = new HomeFragment();
//            imageView.setVisibility(View.VISIBLE);
            textViewTitle.setText(getString(R.string.app_name));
        } else if (id == R.id.action_order) {
            fragment = new OrderFragment();
//            imageView.setVisibility(View.GONE);
            textViewTitle.setText(getString(R.string.order));
        } else if (id == R.id.action_notification) {
            fragment = new NotificationFragment();
//            imageView.setVisibility(View.GONE);
            textViewTitle.setText(getString(R.string.notification));
        } else if (id == R.id.action_account) {
            fragment = new AccountFragment();
            textViewTitle.setText(getString(R.string.account));
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_home, fragment);
            fragmentTransaction.commit();
        }

        return true;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.white));

        LinearLayout linearLayout = toolbar.findViewById(R.id.linear);
        linearLayout.setGravity(Gravity.CENTER);

        textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);

        imageView = toolbar.findViewById(R.id.image);
        imageView.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        getSupportFragmentManager().beginTransaction().add(R.id.frame_home, new HomeFragment()).commit();
    }

    /**
     * 0 Navigation Helper
     */
    private static class BottomNavigationViewHelper {
        @SuppressLint("RestrictedApi")
        static void disableShiftMode(BottomNavigationView view) {
            BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
            try {
                Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
                shiftingMode.setAccessible(true);
                shiftingMode.setBoolean(menuView, false);
                shiftingMode.setAccessible(false);
                for (int i = 0; i < menuView.getChildCount(); i++) {
                    BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                    //noinspection RestrictedApi
                    item.setShiftingMode(false);
                    // set once again checked value, so view will be updated
                    //noinspection RestrictedApi
                    item.setChecked(item.getItemData().isChecked());
                }
            } catch (NoSuchFieldException e) {
                Log.e("BottomNavigation : ", "Unable to get shift mode field", e);
            } catch (IllegalAccessException e) {
                Log.e("BottomNavigation : ", "Unable to change value of shift mode", e);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        LocalStorage localStorage = new LocalStorage(MainActivity.this);

        if (item.getItemId() == R.id.action_currency) {
            startActivity(new Intent(this, CurencyActivity.class));
        }

        if (item.getItemId() == R.id.action_language) {
            ViewHelper.showDialogLeanguage(localStorage, this, new ViewHelper.OnCallbackDialogTitle() {
                @Override
                public void onSelected(String value) {
                    Locale locale;
                    Log.d("language", value);
                    if (value.equals("Indonesia")) {
                        localStorage.setLanguage("in");
                        Locale.setDefault(new Locale("in"));
                        locale = new Locale("in");
                    } else {
                        localStorage.setLanguage("en");
                        Locale.setDefault(Locale.ENGLISH);
                        locale = new Locale("en");
                    }

                    Configuration config = getBaseContext().getResources().getConfiguration();

                    Locale.setDefault(locale);
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

                    Intent refresh = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(refresh);
                    finish();
                }
            });
        }

        return super.onOptionsItemSelected(item);
    }
}
