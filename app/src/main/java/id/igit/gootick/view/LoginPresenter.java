package id.igit.gootick.view;

import android.os.Handler;
import android.os.Looper;

import id.igit.gootick.entity.account.Data;
import id.igit.gootick.entity.account.User;
import id.igit.gootick.service.BaseApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Kristiawan on 05/09/18.
 */
public class LoginPresenter {

    private LoginView view;
    private BaseApiService apiService;

    public LoginPresenter(LoginView view) {
        this.view = view;
        this.apiService = BaseApiService.newInstance();
    }

    public void login(Data user) {
        view.showProgress();
        apiService.getAccountService().login(user)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            if (response.isSuccessful()) {
                                if (response.body().getData() != null) {
                                    view.onLoginSuccess(response.body());
                                } else {
                                    view.onLoadError(response.body().getMessage());
                                }

                            }

                            view.removeProgress();
                        });
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            view.onLoadError(t.getMessage());
                            view.removeProgress();
                        });
                    }
                });
    }
}
