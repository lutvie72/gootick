package id.igit.gootick.view.home;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.App;
import id.igit.gootick.R;
import id.igit.gootick.helper.ViewHelper;

public class FormPassengerActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.edit_title)
    TextInputEditText editTextTitle;
    @BindView(R.id.edit_name)
    TextInputEditText editTextName;
    @BindView(R.id.edit_identity)
    TextInputEditText editTextIdentity;
    @BindView(R.id.layout_identity)
    TextInputLayout layoutIdentity;
    @BindView(R.id.layout_birthday)
    TextInputLayout layoutBirthDay;
    @BindView(R.id.button_save)
    Button buttonSave;

    private String type = "";
    private int number = 0;
    private Map<String, String> params = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_passenger);
        ButterKnife.bind(this);

        type = getIntent().getStringExtra("type");
        params = (Map<String, String>) App.getInstance().fromJSON(getIntent().getStringExtra("data"), params.getClass());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        if (type.equals("adult")) {
            textViewTitle.setText("Adult Passenger Details");
            editTextName.setText(params.get("firstnamea" + number + "") + " " + params.get("lastnamea" + number + ""));
        } else if (type.equals("child")) {
            textViewTitle.setText("Child Passenger Details");
            layoutIdentity.setVisibility(View.GONE);
            layoutBirthDay.setVisibility(View.VISIBLE);
            editTextName.setText(params.get("firstnamec" + number + "") + " " + params.get("lastnamec" + number + ""));
        } else {
            textViewTitle.setText("infant Passenger Details");
            layoutIdentity.setVisibility(View.GONE);
            layoutBirthDay.setVisibility(View.VISIBLE);
        }
        editTextTitle.setOnClickListener(this);
        buttonSave.setOnClickListener(this);

        textViewTitle.setTextSize(20);
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        textViewTitle.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == editTextTitle.getId()) {
            if (type.equals("adult")) {
                ViewHelper.showDialogTitleAdult(this, value -> editTextTitle.setText(value));
            } else if (type.equals("child")) {
                ViewHelper.showDialogTitleChild(this, value -> editTextTitle.setText(value));
            }

        }

        if (view.getId() == buttonSave.getId()) {
            String[] names = editTextName.getText().toString().split(" ");
            String lastname = "";
            for (String n : names) {
                lastname = lastname + " " + n;
            }

            if (type.equals("adult")) {
                params.put("titlea" + number + "","");
                params.put("firstnamea" + number + "", names[0]);
                params.put("lastnamea" + number + "", lastname);
                params.put("ida" + number + "", "");
            } else if (type.equals("child")) {
                params.put("titlec" + number + "","");
                params.put("firstnamec" + number + "", names[0]);
                params.put("lastnamec" + number + "", lastname);
                params.put("birthdatec" + number + "", "");
            }else if (type.equals("infant")) {
                params.put("titlei" + number + "","");
                params.put("firstnamei" + number + "", names[0]);
                params.put("lastnamei" + number + "", lastname);
                params.put("birthdatei" + number + "", "");
                params.put("parenti" + number + "", "");
            }

        }
    }
}
