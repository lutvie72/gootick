package id.igit.gootick.view.home.hotel;

import android.os.Handler;
import android.os.Looper;

import id.igit.gootick.entity.hoteldata.SearchHotel;
import id.igit.gootick.entity.hoteldata.gootick.search.HotelSearch;
import id.igit.gootick.service.BaseApiService;
import id.igit.gootick.view.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Kristiawan on 29/04/18.
 */
public class SearchAutocompletePresenter implements BasePresenter<String> {

    private SearchAutocompleteView view;
    private BaseApiService apiService;

    public SearchAutocompletePresenter(SearchAutocompleteView view) {
        this.view = view;
        apiService = BaseApiService.newInstance();
    }

    @Override
    public void find(String keyword) {
        view.showProgress();
        apiService.getHotelService().searchHotelAutocomplete(keyword)
                .enqueue(new Callback<HotelSearch>() {
                    @Override
                    public void onResponse(Call<HotelSearch> call, Response<HotelSearch> response) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    view.onLoadSuccess(response.body());
                                }

                                view.removeProgress();
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<HotelSearch> call, Throwable t) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                view.onLoadError(t.getMessage());
                                view.removeProgress();
                            }
                        });
                    }
                });
    }

    @Override
    public void findById(String param) {

    }
}
