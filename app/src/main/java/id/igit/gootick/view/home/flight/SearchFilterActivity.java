package id.igit.gootick.view.home.flight;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;

import id.igit.gootick.entity.airplanedata.airport.BaseAirport;
import id.igit.gootick.entity.airplanedata.airport.gootick.Airport;
import id.igit.gootick.entity.airplanedata.airport.gootick.Datum;
import id.igit.gootick.view.home.SimpleDividerItemDecoration;
import id.igit.gootick.view.home.adapter.SearchRecyclerAdapter;

public class SearchFilterActivity extends AppCompatActivity implements SearchFilterView {

    @BindView(R.id.recycler_autocomplete)
    RecyclerView recyclerView;

    private List<id.igit.gootick.entity.airplanedata.airport.Airport> airports;
    private SearchRecyclerAdapter adapter;
    @BindView(R.id.layout_progress)
    RelativeLayout layoutProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_search);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        EditText editTextSearch = toolbar.findViewById(R.id.search);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        airports = new ArrayList<>();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        adapter = new SearchRecyclerAdapter(airports, getIntent().getStringExtra("type"));
        recyclerView.setAdapter(adapter);
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        SearchFilterPresenter presenter = new SearchFilterPresenter(this);
        presenter.find(null);
    }

    @Override
    public void showProgress() {
        layoutProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeProgress() {
        layoutProgress.setVisibility(View.GONE);
    }

    @Override
    public void onLoadError(String error) {

    }

    @Override
    public void onLoadSuccess(BaseAirport result) {
        airports.clear();
        airports.addAll(result.getAllAirport().getAirport());
        adapter.notifyDataSetChanged();
    }
}
