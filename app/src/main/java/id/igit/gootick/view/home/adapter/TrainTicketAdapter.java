package id.igit.gootick.view.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.entity.traindata.Result;
import id.igit.gootick.entity.traindata.gootick.Datum;
import id.igit.gootick.util.LocalStorage;
import id.igit.gootick.view.home.train.DetailInfoTicketActivity;
import id.igit.gootick.view.home.train.DetailTrainTicketActivity;

/**
 * Created by Kristiawan on 30/04/18.
 */
public class TrainTicketAdapter extends RecyclerView.Adapter<TrainTicketAdapter.ViewHolder> {

    private Context context;
    private List<Datum> trainTickets;
    LocalStorage localStorage;

    public TrainTicketAdapter(Context context, List<Datum> trainTickets) {
        this.context = context;
        this.trainTickets = trainTickets;
        localStorage = new LocalStorage(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_ticket_train, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Datum trainTicket = trainTickets.get(position);
        holder.textViewTitle.setText(trainTicket.getTrainName());
        holder.textViewDestination.setText(trainTicket.getDepartureStation() + " - " + trainTicket.getArrivalStation());
        holder.textViewPrice.setText(localStorage.getCurrency().toUpperCase() + " " + trainTicket.getPriceTotalClean());
        holder.textViewTime.setText(trainTicket.getDepartureTime() + " - " + trainTicket.getArrivalTime());
        holder.textViewDuration.setText(trainTicket.getDuration() + " | " + trainTicket.getClassNameLang());
    }

    @Override
    public int getItemCount() {
        return trainTickets.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.text_title)
        TextView textViewTitle;
        @BindView(R.id.text_destination)
        TextView textViewDestination;
        @BindView(R.id.text_price)
        TextView textViewPrice;
        @BindView(R.id.text_time)
        TextView textViewTime;
        @BindView(R.id.text_duration)
        TextView textViewDuration;
        @BindView(R.id.text_detail)
        TextView textViewDetail;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            textViewDetail.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == textViewDetail.getId()) {
                context.startActivity(new Intent(context, DetailInfoTicketActivity.class));
            } else {
                context.startActivity(new Intent(context, DetailTrainTicketActivity.class));
            }
        }
    }
}
