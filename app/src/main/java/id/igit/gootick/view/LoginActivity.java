package id.igit.gootick.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.entity.account.Data;
import id.igit.gootick.entity.account.User;
import id.igit.gootick.helper.ViewHelper;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, LoginView {

    @BindView(R.id.edit_email)
    EditText editTextEmail;
    @BindView(R.id.edit_password)
    EditText editTextPassword;
    @BindView(R.id.text_register)
    TextView textViewRegister;
    @BindView(R.id.button_sigin)
    Button buttonSigin;

    LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        presenter = new LoginPresenter(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText(getString(R.string.sigin));
        textViewTitle.setTextSize(20);
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        textViewRegister.setOnClickListener(this);
        buttonSigin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == textViewRegister.getId()) {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);
        }

        if (view.getId() == buttonSigin.getId()) {
            Data user = new Data();
            user.setEmail(editTextEmail.getText().toString());
            user.setPassword(editTextPassword.getText().toString());

            if (TextUtils.isEmpty(editTextEmail.getText().toString())
                    || TextUtils.isEmpty(editTextPassword.getText().toString())) {

                ViewHelper.showMessage(this, getString(R.string.message_field_empty));
            } else {
                presenter.login(user);
            }
        }
    }

    @Override
    public void onLoginSuccess(User user) {
        ViewHelper.showMessage(this, user.getMessage());
        finish();
    }

    @Override
    public void showProgress() {
        ViewHelper.getInstance().showProgressDialog(this);
    }

    @Override
    public void removeProgress() {
        ViewHelper.getInstance().dismisProgressDialog();
    }

    @Override
    public void onLoadError(String error) {
        ViewHelper.showMessage(this, error);
    }

    @Override
    public void onLoadSuccess(User result) {

    }
}
