package id.igit.gootick.view.home.train;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.App;
import id.igit.gootick.R;
import id.igit.gootick.entity.traindata.SearchQueries;
import id.igit.gootick.entity.traindata.gootick.Datum;
import id.igit.gootick.entity.traindata.gootick.TrainTiket;
import id.igit.gootick.helper.GeneraleHelper;
import id.igit.gootick.helper.ViewHelper;
import id.igit.gootick.util.LocalStorage;
import id.igit.gootick.view.home.adapter.TrainTicketAdapter;

public class TrainTicketActivity extends AppCompatActivity implements View.OnClickListener, TrainTicketView {

    @BindView(R.id.recycler_ticket)
    RecyclerView recyclerView;
    @BindView(R.id.button_change_search)
    Button buttonChangeSearch;
    @BindView(R.id.text_destination)
    TextView textViewDestination;
    @BindView(R.id.text_guest)
    TextView textViewGuest;
    @BindView(R.id.text_start_date)
    TextView textViewStartDate;
    @BindView(R.id.text_end_date)
    TextView textViewEndDate;

    private List<Datum> trainTickets;
    private TrainTicketPresenter presenter;
    private LocalStorage localStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_ticket);
        ButterKnife.bind(this);

        localStorage = new LocalStorage(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText("Hasil Pencarian");
        textViewTitle.setTextSize(20);
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        buttonChangeSearch.setOnClickListener(this);

        trainTickets = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new TrainTicketAdapter(this, trainTickets));

        SearchQueries searchQueries = (SearchQueries) App.getInstance().fromJSON(getIntent().getStringExtra("param"), SearchQueries.class);
        textViewDestination.setText(searchQueries.getmFrom() + "-" + searchQueries.getmTo());
        textViewGuest.setText(searchQueries.getAdult() + " " + getString(R.string.adult) + " 0 " + getString(R.string.children) + " " + searchQueries.getInfant() + " " + getString(R.string.infant));
        textViewStartDate.setText(GeneraleHelper.convertDate(GeneraleHelper.convertStringToDate(searchQueries.getDate()), GeneraleHelper.SHORT_TEXT_DATE_FORMAT, new Locale(localStorage.getLanguage().toLowerCase())));
        if (searchQueries.getRetDate() != null){
            textViewEndDate.setText(GeneraleHelper.convertDate(GeneraleHelper.convertStringToDate(searchQueries.getRetDate()), GeneraleHelper.SHORT_TEXT_DATE_FORMAT, new Locale(localStorage.getLanguage().toLowerCase())));

        }else {
            textViewEndDate.setText("-");

        }

        presenter = new TrainTicketPresenter(this);
        presenter.find(searchQueries);
    }

    @Override
    public void showProgress() {
        ViewHelper.getInstance().showProgressDialog(this);
    }

    @Override
    public void removeProgress() {
        ViewHelper.getInstance().dismisProgressDialog();
    }

    @Override
    public void onLoadError(String error) {
        Toast.makeText(this, R.string.message_error_connection_server, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLoadSuccess(TrainTiket result) {
        trainTickets.clear();
        if (result.getData() != null) {
            if (result.getData().isEmpty()){
                Toast.makeText(this, R.string.message_ticket_not_available, Toast.LENGTH_LONG).show();
            }

            trainTickets.addAll(result.getData());
            recyclerView.getAdapter().notifyDataSetChanged();
        }

//        textViewDestination.setText(result.getQueryParams().getArrStation() + "-" + result.getQueryParams().getDepStation());
//        textViewGuest.setText(result.getQueryParams().getCountAdult() + " " + getString(R.string.adult) + " 0 " + getString(R.string.children) + " " + result.getQueryParams().getCountInfant() + " " + getString(R.string.infant));
//        textViewStartDate.setText(GeneraleHelper.convertDate(GeneraleHelper.convertStringToDate(result.getQueryParams().getDate()), GeneraleHelper.SHORT_TEXT_DATE_FORMAT, new Locale(localStorage.getLanguage().toLowerCase())));
//        textViewEndDate.setText(GeneraleHelper.convertDate(GeneraleHelper.convertStringToDate(result.getQueryParams().getReturnDate()), GeneraleHelper.SHORT_TEXT_DATE_FORMAT, new Locale(localStorage.getLanguage().toLowerCase())));

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == buttonChangeSearch.getId()) {
            finish();
        }
    }
}
