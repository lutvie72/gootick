package id.igit.gootick.view.home.hotel;

import id.igit.gootick.entity.hoteldata.SearchHotel;
import id.igit.gootick.entity.hoteldata.gootick.search.HotelSearch;
import id.igit.gootick.view.BaseView;

/**
 * Created by Kristiawan on 29/04/18.
 */
public interface SearchAutocompleteView extends BaseView<HotelSearch> {
}
