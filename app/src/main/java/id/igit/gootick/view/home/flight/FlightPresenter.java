package id.igit.gootick.view.home.flight;

import android.os.Handler;
import android.os.Looper;

import id.igit.gootick.entity.airplanedata.gotick.PlaneTiket;
import id.igit.gootick.entity.airplanedata.SearchQueries;
import id.igit.gootick.service.BaseApiService;
import id.igit.gootick.view.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Kristiawan on 07/04/18.
 */

public class FlightPresenter implements BasePresenter<SearchQueries> {

    private FlightView view;
    private BaseApiService apiService;

    public FlightPresenter(FlightView view) {
        this.view = view;
        apiService = BaseApiService.newInstance();
    }

    @Override
    public void find(SearchQueries param) {
        view.showProgress();
        apiService.getFlightService().find(param)
                .enqueue(new Callback<PlaneTiket>() {
                    @Override
                    public void onResponse(Call<PlaneTiket> call, Response<PlaneTiket> response) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            if (response.isSuccessful()) {
                                view.onLoadSuccess(response.body());
                            } else {
                                view.onLoadError(response.message());
                            }

                            view.removeProgress();
                        });
                    }

                    @Override
                    public void onFailure(Call<PlaneTiket> call, Throwable t) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            view.onLoadError(t.getMessage());
                            view.removeProgress();
                        });
                    }
                });
    }

    @Override
    public void findById(SearchQueries param) {

    }
}
