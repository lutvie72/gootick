package id.igit.gootick.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.entity.account.Data;
import id.igit.gootick.entity.account.User;
import id.igit.gootick.helper.ViewHelper;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, RegisterView {

    @BindView(R.id.edit_fullname)
    EditText editTextFullname;
    @BindView(R.id.edit_email)
    EditText editTextEmail;
    @BindView(R.id.edit_no_telp)
    EditText editTextNoTelp;
    @BindView(R.id.edit_password)
    EditText editTextPassword;
    @BindView(R.id.edit_confirm_password)
    EditText editTextConfirmPassword;
    @BindView(R.id.button_register)
    Button buttonRegister;

    RegisterPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        presenter = new RegisterPresenter(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText(getString(R.string.signup));
        textViewTitle.setTextSize(20);
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        buttonRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == buttonRegister.getId()) {
            Data user = new Data();
            user.setName(editTextFullname.getText().toString());
            user.setEmail(editTextEmail.getText().toString());
            user.setMobile(editTextNoTelp.getText().toString());
            user.setPassword(editTextPassword.getText().toString());
            user.setPassConfirm(editTextConfirmPassword.toString());

            if (TextUtils.isEmpty(editTextFullname.getText().toString())
                    || TextUtils.isEmpty(editTextEmail.getText().toString())
                    || TextUtils.isEmpty(editTextNoTelp.getText().toString())
                    || TextUtils.isEmpty(editTextPassword.getText().toString())
                    || TextUtils.isEmpty(editTextConfirmPassword.toString())) {

                ViewHelper.showMessage(this, getString(R.string.message_field_empty));
            } else if (!TextUtils.equals(editTextPassword.getText().toString(), editTextConfirmPassword.toString())) {
                ViewHelper.showMessage(this, getString(R.string.message_password_same));
            } else {
                presenter.register(user);
            }

        }
    }

    @Override
    public void onReqisterSuccess(User user) {
        ViewHelper.showMessage(this, user.getMessage());
        finish();
    }

    @Override
    public void showProgress() {
        ViewHelper.getInstance().showProgressDialog(this);
    }

    @Override
    public void removeProgress() {
        ViewHelper.getInstance().dismisProgressDialog();
    }

    @Override
    public void onLoadError(String error) {
        ViewHelper.showMessage(this, error);
    }

    @Override
    public void onLoadSuccess(User result) {

    }
}
