package id.igit.gootick.view;

import id.igit.gootick.entity.account.User;

/**
 * Created by Kristiawan on 05/09/18.
 */
public interface RegisterView extends BaseView<User> {

    void onReqisterSuccess(User user);
}
