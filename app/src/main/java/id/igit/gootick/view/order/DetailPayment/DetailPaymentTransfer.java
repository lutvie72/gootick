package id.igit.gootick.view.order.DetailPayment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.diegodobelo.expandingview.ExpandingItem;
import com.diegodobelo.expandingview.ExpandingList;

import id.igit.gootick.R;

public class DetailPaymentTransfer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_payment_transfer);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        textViewTitle.setText("Bank Transfer");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        ExpandingList expandingList = (ExpandingList) findViewById(R.id.expanding_list_main);
        ExpandingItem item = expandingList.createNewItem(R.layout.expanding_layout);
        ExpandingItem item1 = expandingList.createNewItem(R.layout.expanding_layout);
        ExpandingItem item2 = expandingList.createNewItem(R.layout.expanding_layout);

        ((TextView) item.findViewById(R.id.title)).setText("ATM BCA");
        ((TextView) item1.findViewById(R.id.title)).setText("Klik BCA");
        ((TextView) item2.findViewById(R.id.title)).setText("m-BCA");

        item.createSubItems(6);
        item1.createSubItems(6);
        item2.createSubItems(6);

        ((TextView) item.getSubItemView(0).findViewById(R.id.sub_title)).setText("Pada menu utama, pilih Transaksi lainya");
        ((TextView) item.getSubItemView(1).findViewById(R.id.sub_title)).setText("Pilih Transfer");
        ((TextView) item.getSubItemView(2).findViewById(R.id.sub_title)).setText("Pilih BCA Virtual Account");
        ((TextView) item.getSubItemView(3).findViewById(R.id.sub_title)).setText("Masukkan Virtual Account Number 7800 1010 01227 1655 , tekan Benar");
        ((TextView) item.getSubItemView(4).findViewById(R.id.sub_title)).setText("Masukkan harga total yang harus dibayar, teken Benar");
        ((TextView) item.getSubItemView(5).findViewById(R.id.sub_title)).setText("Tekan Ya jika detail pembayaran benar");

        ((TextView) item1.getSubItemView(0).findViewById(R.id.sub_title)).setText("Pada menu utama, pilih Transaksi lainya");
        ((TextView) item1.getSubItemView(1).findViewById(R.id.sub_title)).setText("Pilih Transfer");
        ((TextView) item1.getSubItemView(2).findViewById(R.id.sub_title)).setText("Pilih BCA Virtual Account");
        ((TextView) item1.getSubItemView(3).findViewById(R.id.sub_title)).setText("Masukkan Virtual Account Number 7800 1010 01227 1655 , tekan Benar");
        ((TextView) item1.getSubItemView(4).findViewById(R.id.sub_title)).setText("Masukkan harga total yang harus dibayar, teken Benar");
        ((TextView) item1.getSubItemView(5).findViewById(R.id.sub_title)).setText("Tekan Ya jika detail pembayaran benar");

        ((TextView) item2.getSubItemView(0).findViewById(R.id.sub_title)).setText("Pada menu utama, pilih Transaksi lainya");
        ((TextView) item2.getSubItemView(1).findViewById(R.id.sub_title)).setText("Pilih Transfer");
        ((TextView) item2.getSubItemView(2).findViewById(R.id.sub_title)).setText("Pilih BCA Virtual Account");
        ((TextView) item2.getSubItemView(3).findViewById(R.id.sub_title)).setText("Masukkan Virtual Account Number 7800 1010 01227 1655 , tekan Benar");
        ((TextView) item2.getSubItemView(4).findViewById(R.id.sub_title)).setText("Masukkan harga total yang harus dibayar, teken Benar");
        ((TextView) item2.getSubItemView(5).findViewById(R.id.sub_title)).setText("Tekan Ya jika detail pembayaran benar");
    }
}
