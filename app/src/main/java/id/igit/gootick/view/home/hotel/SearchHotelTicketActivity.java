package id.igit.gootick.view.home.hotel;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.App;
import id.igit.gootick.R;
import id.igit.gootick.entity.hoteldata.ResultSearchHotel;
import id.igit.gootick.entity.hoteldata.SearchQueries;
import id.igit.gootick.entity.hoteldata.gootick.search.Result;
import id.igit.gootick.helper.GeneraleHelper;
import id.igit.gootick.helper.ViewHelper;
import id.igit.gootick.util.EventRxBus;
import id.igit.gootick.util.LocalStorage;
import id.igit.gootick.view.CalendarActivity;
import id.igit.gootick.view.home.FormGuestActivity;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class SearchHotelTicketActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.button_search)
    Button buttonSearch;
    @BindView(R.id.edit_destination)
    TextView textViewDestination;
    @BindView(R.id.edit_check_in)
    TextView textViewCheckIn;
    @BindView(R.id.edit_check_out)
    TextView textViewCeckOut;
    @BindView(R.id.edit_guest)
    TextView textViewGuest;
    @BindView(R.id.edit_room)
    TextView textViewRoom;

    private LocalStorage localStorage;

    private SearchQueries searchQueries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_hotel_ticket);
        ButterKnife.bind(this);
        localStorage = new LocalStorage(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText(getIntent().getStringExtra("title"));
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        textViewTitle.setTextSize(20);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        buttonSearch.setOnClickListener(this);
        textViewDestination.setOnClickListener(this);
        textViewCheckIn.setOnClickListener(this);
        textViewCeckOut.setOnClickListener(this);
        textViewGuest.setOnClickListener(this);
        textViewRoom.setOnClickListener(this);

        textViewGuest.setText("1 "+getString(R.string.adult)+" 0 "+getString(R.string.children));
        textViewRoom.setText("1 "+getString(R.string.room));

        searchQueries = new SearchQueries();
        searchQueries.setAdult("0");
        searchQueries.setChild(0);
        searchQueries.setRoom("1");
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventRxBus.getInstance().getEvents().subscribe(new Observer<Object>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onNext(Object value) {
                if (value instanceof ArrayList) {
                    List<Date> dates = (List<Date>) value;

                    if (dates.size() == 1) {
                        textViewCheckIn.setText(GeneraleHelper.convertDate(dates.get(0), GeneraleHelper.SHORT_TEXT_DATE_FORMAT));
                        searchQueries.setStartdate(GeneraleHelper.convertDate(dates.get(0), GeneraleHelper.DEFAULT_DATE_FORMAT));
                    } else {
                        textViewCeckOut.setText(GeneraleHelper.convertDate(dates.get(1), GeneraleHelper.SHORT_TEXT_DATE_FORMAT));
                        searchQueries.setEnddate(GeneraleHelper.convertDate(dates.get(1), GeneraleHelper.DEFAULT_DATE_FORMAT));
                    }
                } else if (value instanceof HashMap) {
                    Map<String, Integer> map = (Map<String, Integer>) value;
                    textViewGuest.setText(map.get("adults") + " "+getString(R.string.adult)+" " + map.get("children") + R.string.children);

                    searchQueries.setAdult(String.valueOf(map.get("adults")));
                    searchQueries.setChild(map.get("children"));
                } else if (value instanceof Result) {
                    Result hotel = (Result) value;
                    searchQueries.setQ(hotel.getValue());
                    textViewDestination.setText(hotel.getValue());
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == buttonSearch.getId()) {
            searchQueries.setCurrency(localStorage.getCurrency());
            searchQueries.setLanguage(localStorage.getLanguage().toUpperCase());

            System.out.println("night days " + searchQueries.getNight());
            if (searchQueries.getStartdate() != null && searchQueries.getEnddate() != null) {
                searchQueries.setNight(
                        GeneraleHelper.countDays(
                                GeneraleHelper.convertStringToDate(searchQueries.getStartdate()),
                                GeneraleHelper.convertStringToDate(searchQueries.getEnddate())) + "");
            }

            if (validation()) {
                Intent intent = new Intent(this, HotelTicketActivity.class);
                intent.putExtra("param", App.getInstance().toJSON(searchQueries));
                startActivity(intent);
            }
        }

        if (view.getId() == textViewDestination.getId()) {
            Intent intent = new Intent(this, SearchAutocompleteActivity.class);
            intent.putExtra("type", "from");
            startActivity(intent);
        }

        if (view.getId() == textViewCheckIn.getId()) {
            Intent intent = new Intent(this, CalendarActivity.class);
            intent.putExtra("mode", "start");
            startActivity(intent);
        }

        if (view.getId() == textViewCeckOut.getId()) {
            Intent intent = new Intent(this, CalendarActivity.class);
            intent.putExtra("mode", "end");
            startActivity(intent);
        }

        if (view.getId() == textViewGuest.getId()) {
            Intent intent = new Intent(this, FormGuestActivity.class);
            intent.putExtra("mode", 1);
            startActivity(intent);
        }

        if (view.getId() == textViewRoom.getId()) {
            ViewHelper.showDialogRoom(this, new ViewHelper.OnCallbackDialogRoom() {
                @Override
                public void onSave(int count) {
                    searchQueries.setRoom(count + "");
                    textViewRoom.setText(count + " " + getString(R.string.room));
                }
            });
        }
    }

    private boolean validation() {
        if ((searchQueries == null) ||
                searchQueries.getQ() == null ||
                searchQueries.getRoom() == null ||
                searchQueries.getStartdate() == null ||
                searchQueries.getEnddate() == null ||
                searchQueries.getAdult() == null) {

            ViewHelper.showMessage(this, getString(R.string.message_field_empty));
            return false;
        } else if (Integer.valueOf(searchQueries.getNight()) < 1) {
            ViewHelper.showMessage(this, getString(R.string.min_one_night));
            return false;
        } else {
            return true;
        }
    }
}
