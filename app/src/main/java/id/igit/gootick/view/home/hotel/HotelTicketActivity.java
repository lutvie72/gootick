package id.igit.gootick.view.home.hotel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.App;
import id.igit.gootick.R;
import id.igit.gootick.entity.hoteldata.HotelTicket;
import id.igit.gootick.entity.hoteldata.Result;
import id.igit.gootick.entity.hoteldata.SearchQueries;
import id.igit.gootick.entity.hoteldata.gootick.Datum;
import id.igit.gootick.entity.hoteldata.gootick.HotelTiket;
import id.igit.gootick.helper.GeneraleHelper;
import id.igit.gootick.helper.ViewHelper;
import id.igit.gootick.util.LocalStorage;
import id.igit.gootick.view.home.adapter.HotelTicketAdapter;

public class HotelTicketActivity extends AppCompatActivity implements View.OnClickListener, HotelView {

    @BindView(R.id.recycler_ticket)
    RecyclerView recyclerView;
    @BindView(R.id.button_change_search)
    Button buttonChangeSearch;

    @BindView(R.id.text_destination)
    TextView textViewDestination;
    @BindView(R.id.text_guest)
    TextView textViewGuest;
    @BindView(R.id.text_start_date)
    TextView textViewStartDate;
    @BindView(R.id.text_end_date)
    TextView textViewEndDate;
    @BindView(R.id.text_room)
    TextView textViewRoom;
    List<Datum> results;
    LocalStorage localStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_ticket);
        ButterKnife.bind(this);
        localStorage = new LocalStorage(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText("Hasil Pencarian");
        textViewTitle.setTextSize(20);
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        buttonChangeSearch.setOnClickListener(this);


        results = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new HotelTicketAdapter(this, results));
        HotelPresenter presenter = new HotelPresenter(this);

        SearchQueries searchQueries = (SearchQueries) App.getInstance().fromJSON(getIntent().getStringExtra("param"), SearchQueries.class);
        textViewDestination.setText(searchQueries.getQ());
        textViewGuest.setText(searchQueries.getAdult() + " " + getString(R.string.adult)+" " + searchQueries.getChild()+" " + getString(R.string.children));
        textViewStartDate.setText(GeneraleHelper.convertDate(GeneraleHelper.convertStringToDate(searchQueries.getStartdate()), GeneraleHelper.SHORT_TEXT_DATE_FORMAT, new Locale(localStorage.getLanguage().toLowerCase())));
        textViewEndDate.setText(GeneraleHelper.convertDate(GeneraleHelper.convertStringToDate(searchQueries.getEnddate()), GeneraleHelper.SHORT_TEXT_DATE_FORMAT, new Locale(localStorage.getLanguage().toLowerCase())));

        presenter.find(searchQueries);
    }

    @Override
    public void showProgress() {
        ViewHelper.getInstance().showProgressDialog(this);
    }

    @Override
    public void removeProgress() {
        ViewHelper.getInstance().dismisProgressDialog();
    }

    @Override
    public void onLoadError(String error) {
        Toast.makeText(this, R.string.message_error_connection_server, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLoadSuccess(HotelTiket result) {
        results.clear();
        if (result != null) {
            if (result.getData() != null) {
                if (result.getData().isEmpty())
                    Toast.makeText(this, R.string.message_ticket_not_available, Toast.LENGTH_LONG).show();

                results.addAll(result.getData());
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == buttonChangeSearch.getId()) {
            finish();
        }
    }
}
