package id.igit.gootick.view.home.flight;

import android.os.Handler;
import android.os.Looper;

import id.igit.gootick.entity.airplanedata.gotick.detail.DetailFlight;
import id.igit.gootick.service.BaseApiService;
import id.igit.gootick.view.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Kristiawan on 15/09/18.
 */
public class DetailInfoFlightTicketPresenter implements BasePresenter<DetailFlight> {

    private DetailInfoFlightTicketView view;
    private BaseApiService apiService;

    public DetailInfoFlightTicketPresenter(DetailInfoFlightTicketView view) {
        this.view = view;
        apiService = BaseApiService.newInstance();
    }

    @Override
    public void find(DetailFlight param) {

    }

    @Override
    public void findById(DetailFlight param) {

    }

    public void findById(String flight_id) {
        view.showProgress();
        apiService.getFlightService().findById(flight_id)
                .enqueue(new Callback<DetailFlight>() {
                    @Override
                    public void onResponse(Call<DetailFlight> call, Response<DetailFlight> response) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    view.onLoadSuccess(response.body());
                                }

                                view.removeProgress();
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<DetailFlight> call, Throwable t) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                view.onLoadError(t.getMessage());
                                view.removeProgress();
                            }
                        });
                    }
                });
    }
}
