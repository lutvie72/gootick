package id.igit.gootick.view.home;

import java.util.List;

import id.igit.gootick.entity.airplanedata.airport.Airport;
import id.igit.gootick.entity.hoteldata.Result;

/**
 * Created by Kristiawan on 07/04/18.
 */

public interface HomeView {

    void onLoadImageSliders(List<String> images);

    void onLoadAirportPopular(List<Airport> airports);

    void onLoadHotelPromo(List<Result> hotels);

    void showProgress();

    void removeProgress();

    void onLoadError();
}
