package id.igit.gootick.view.home.flight;

import id.igit.gootick.entity.airplanedata.gotick.detail.DetailFlight;
import id.igit.gootick.view.BaseView;

/**
 * Created by Kristiawan on 15/09/18.
 */
public interface DetailInfoFlightTicketView extends BaseView<DetailFlight> {
}
