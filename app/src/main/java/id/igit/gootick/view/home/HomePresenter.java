package id.igit.gootick.view.home;

import android.os.Handler;
import android.os.Looper;

import id.igit.gootick.entity.airplanedata.airport.BaseAirport;
import id.igit.gootick.entity.hoteldata.HotelTicket;
import id.igit.gootick.service.BaseApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Kristiawan on 07/04/18.
 */

public class HomePresenter {

    private HomeView view;
    private BaseApiService apiService;

    public HomePresenter(HomeView view) {
        this.view = view;
        this.apiService = BaseApiService.newInstance();
    }

    public void findAirportPopular() {
        apiService.getFlightService()
                .findAitportPopular()
                .enqueue(new Callback<BaseAirport>() {
                    @Override
                    public void onResponse(Call<BaseAirport> call, Response<BaseAirport> response) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            if (response.isSuccessful()) {
                                if (response.body().getPopularDestinations() != null)
                                    view.onLoadAirportPopular(response.body().getPopularDestinations().getAirport());
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<BaseAirport> call, Throwable t) {
                        new Handler(Looper.getMainLooper()).post(() -> view.onLoadError());
                    }
                });
    }

    public void findHotelPromo() {
        apiService.getHotelService()
                .findHotelPromo()
                .enqueue(new Callback<HotelTicket>() {
                    @Override
                    public void onResponse(Call<HotelTicket> call, Response<HotelTicket> response) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            if (response.isSuccessful()) {
                                if (response.body().getResults() != null)
                                view.onLoadHotelPromo(response.body().getResults().getResult());
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<HotelTicket> call, Throwable t) {
                        new Handler(Looper.getMainLooper()).post(() -> view.onLoadError());
                    }
                });
    }
}
