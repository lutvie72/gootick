package id.igit.gootick.view.home.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.igit.gootick.R;
import id.igit.gootick.entity.Content;
import id.igit.gootick.helper.ImageHelper;
import id.igit.gootick.util.ColorUtil;

/**
 * Created by Kristiawan on 05/04/18.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeHolder> {

    private Context context;
    private List<Content> contents;

    public HomeAdapter(List<Content> contents) {
        this.contents = contents;
    }

    @Override
    public HomeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new HomeHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_home2, parent, false));
    }

    @Override
    public void onBindViewHolder(HomeHolder holder, int position) {
        if (position == 0) {
            holder.itemView.setPadding(50, 0, 0, 0);
        }

        Content content = contents.get(position);

        holder.textView.setText(content.getTitle());
        holder.cardView.setCardBackgroundColor(Color.parseColor(ColorUtil.randomColor()));
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    class HomeHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView textView;

        public HomeHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card);
            textView = itemView.findViewById(R.id.text_title);
        }
    }
}
