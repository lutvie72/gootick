package id.igit.gootick.view.home.flight;

import android.os.Handler;
import android.os.Looper;

import id.igit.gootick.entity.airplanedata.airport.BaseAirport;
import id.igit.gootick.entity.airplanedata.airport.gootick.Airport;
import id.igit.gootick.service.BaseApiService;
import id.igit.gootick.view.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Kristiawan on 29/04/18.
 */
public class SearchFilterPresenter implements BasePresenter<BaseAirport> {

    private SearchFilterView view;
    private BaseApiService apiService;

    public SearchFilterPresenter(SearchFilterView view) {
        this.view = view;
        apiService = BaseApiService.newInstance();
    }

    @Override
    public void find(BaseAirport param) {
        view.showProgress();
        apiService.getFlightService().findAitport()
                .enqueue(new Callback<BaseAirport>() {
                    @Override
                    public void onResponse(Call<BaseAirport> call, Response<BaseAirport> response) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            if (response.isSuccessful()) {
                                view.onLoadSuccess(response.body());
                            }

                            view.removeProgress();
                        });
                    }

                    @Override
                    public void onFailure(Call<BaseAirport> call, Throwable t) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                view.onLoadError(t.getMessage());
                                view.removeProgress();
                            }
                        });
                    }
                });
    }

    @Override
    public void findById(BaseAirport param) {

    }
}
