package id.igit.gootick.view;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.entity.currency.Currency;
import id.igit.gootick.entity.currency.Result;
import id.igit.gootick.helper.ImageHelper;
import id.igit.gootick.helper.ViewHelper;
import id.igit.gootick.service.BaseApiService;
import id.igit.gootick.service.CurrencyApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurencyActivity extends AppCompatActivity {

    @BindView(R.id.rv_curency)
    RecyclerView recyclerView;

    @BindView(R.id.button_save)
    Button buttonSave;

    private List<Result> currencies = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_curency);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText("Currency");
        textViewTitle.setTextSize(20);
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CurrencyAdapter(currencies, this));
        load();
    }

    private void load() {
        ViewHelper.getInstance().showProgressDialog(this);
        BaseApiService.newInstance()
                .getCurrencyService().find()
                .enqueue(new Callback<Currency>() {
                    @Override
                    public void onResponse(Call<Currency> call, Response<Currency> response) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            if (response.isSuccessful()) {
                                if (response.body().getResult() != null)
                                    for (Result result : response.body().getResult()) {
                                        currencies.add(result);
                                    }

                                recyclerView.getAdapter().notifyDataSetChanged();
                            }
                            ViewHelper.getInstance().dismisProgressDialog();
                        });
                    }

                    @Override
                    public void onFailure(Call<Currency> call, Throwable t) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            ViewHelper.showMessage(CurencyActivity.this, t.getMessage());
                            ViewHelper.getInstance().dismisProgressDialog();
                        });
                    }
                });
    }
}