package id.igit.gootick.view.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.entity.airplanedata.Result;
import id.igit.gootick.entity.airplanedata.gotick.Datum;
import id.igit.gootick.entity.airplanedata.gotick.PlaneTiket;
import id.igit.gootick.helper.ImageHelper;
import id.igit.gootick.util.LocalStorage;
import id.igit.gootick.view.home.flight.DetailFlightTicketActivity;
import id.igit.gootick.view.home.flight.DetailInfoFlightTicketActivity;
import id.igit.gootick.view.home.train.DetailInfoTicketActivity;
import id.igit.gootick.view.home.train.DetailTrainTicketActivity;

/**
 * Created by Kristiawan on 07/04/18.
 */

public class FlightTicketAdapter extends RecyclerView.Adapter<FlightTicketAdapter.TicketHolder> {

    Context context;
    List<Datum> airplaneTickets;
    LocalStorage localStorage;

    public FlightTicketAdapter(Context context, List<Datum> airplaneTickets) {
        this.context = context;
        this.airplaneTickets = airplaneTickets;
        localStorage = new LocalStorage(context);
    }

    @Override
    public TicketHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new TicketHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_ticket_flight, parent, false));
    }

    @Override
    public void onBindViewHolder(TicketHolder holder, int position) {
        Datum result = airplaneTickets.get(position);

        ImageHelper.getInstance().setImageFromUrl(context, result.getImage(), holder.imageView);
        holder.textViewTitle.setText(result.getAirlinesName());
        holder.textViewPrice.setText(localStorage.getCurrency().toUpperCase() + " " + result.getPriceValueAf());
        holder.textViewTime.setText(result.getSimpleDepartureTime() + " - " + result.getSimpleArrivalTime());
        holder.textViewDuration.setText(result.getDuration());
    }

    @Override
    public int getItemCount() {
        return airplaneTickets.size();
    }

    class TicketHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.icon)
        ImageView imageView;
        @BindView(R.id.text_title)
        TextView textViewTitle;
        @BindView(R.id.text_price)
        TextView textViewPrice;
        @BindView(R.id.text_time)
        TextView textViewTime;
        @BindView(R.id.text_duration)
        TextView textViewDuration;
        @BindView(R.id.text_detail)
        TextView textViewDetail;

        public TicketHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            textViewDetail.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == textViewDetail.getId()) {
                context.startActivity(new Intent(context, DetailInfoFlightTicketActivity.class));
            } else {
                Intent intent = new Intent(context, DetailFlightTicketActivity.class);
                intent.putExtra("flight_id", airplaneTickets.get(getAdapterPosition()).getFlightId());
                context.startActivity(intent);
            }
        }
    }
}
