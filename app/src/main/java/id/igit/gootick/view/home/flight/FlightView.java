package id.igit.gootick.view.home.flight;

import id.igit.gootick.entity.airplanedata.gotick.PlaneTiket;
import id.igit.gootick.view.BaseView;

/**
 * Created by Kristiawan on 07/04/18.
 */

public interface FlightView extends BaseView<PlaneTiket> {
}
