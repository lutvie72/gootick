package id.igit.gootick.view.home.hotel;

import android.os.Handler;
import android.os.Looper;

import id.igit.gootick.entity.hoteldata.HotelTicket;
import id.igit.gootick.entity.hoteldata.SearchQueries;
import id.igit.gootick.entity.hoteldata.gootick.HotelTiket;
import id.igit.gootick.service.BaseApiService;
import id.igit.gootick.view.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Kristiawan on 17/04/18.
 */

public class HotelPresenter implements BasePresenter<SearchQueries> {

    private HotelView view;
    private BaseApiService apiService;

    public HotelPresenter(HotelView view) {
        this.view = view;
        apiService = BaseApiService.newInstance();
    }

    @Override
    public void find(SearchQueries param) {
        view.showProgress();
        apiService.getHotelService().find(param)
                .enqueue(new Callback<HotelTiket>() {
                    @Override
                    public void onResponse(Call<HotelTiket> call, Response<HotelTiket> response) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            if (response.isSuccessful()) {
                                view.onLoadSuccess(response.body());
                            }

                            view.removeProgress();
                        });
                    }

                    @Override
                    public void onFailure(Call<HotelTiket> call, Throwable t) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            view.onLoadError(t.getMessage());
                            view.removeProgress();
                        });
                    }
                });
    }

    @Override
    public void findById(SearchQueries param) {

    }
}
