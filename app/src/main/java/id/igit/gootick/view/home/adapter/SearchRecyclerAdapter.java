package id.igit.gootick.view.home.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.igit.gootick.R;
import id.igit.gootick.entity.airplanedata.airport.Airport;
import id.igit.gootick.entity.airplanedata.airport.gootick.Datum;
import id.igit.gootick.util.EventRxBus;

/**
 * Created by Kristiawan on 28/04/18.
 */

public class SearchRecyclerAdapter extends RecyclerView.Adapter<SearchRecyclerAdapter.ViewHolder> implements Filterable {
    private List<Airport> airports;
    private List<Airport> filteredList;
    private String type;
    private Context context;

    public SearchRecyclerAdapter(List<Airport> items, String type) {
        airports = items;
        filteredList = items;
        this.type = type;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_search_filter, parent, false);
        if (context == null) {
            this.context = parent.getContext();
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Airport airport = filteredList.get(position);
        holder.textLocation.setText(airport.getLocationName());
        holder.textAirport.setText(airport.getAirportCode() + " - " + airport.getAirportName());
    }

    @Override
    public int getItemCount() {
        return filteredList != null ? filteredList.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList = airports;
                } else {
                    List<Airport> filteredAirports = new ArrayList<>();
                    for (Airport row : airports) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
//                        if (row.getRuang().toLowerCase().startsWith(charString.toLowerCase())) {
//                            filteredList.add(row);
//                        }

                        boolean isfirst = false;

                        for (String s : row.getLocationName().toLowerCase().split(" ")) {
                            if (s.startsWith(charString.toLowerCase())) {
                                filteredAirports.add(row);
                                isfirst = true;
                                break;
                            } else if (row.getLocationName().toLowerCase().startsWith(charString.toLowerCase())) {
                                filteredAirports.add(row);
                                isfirst = true;
                                break;
                            }
                        }

                        if (!isfirst){
                            for (String s : row.getAirportCode().concat(" ").concat(row.getAirportName()).toLowerCase().split(" ")) {
                                if (s.startsWith(charString.toLowerCase())) {
                                    filteredAirports.add(row);
                                    break;
                                } else if (row.getAirportName().toLowerCase().startsWith(charString.toLowerCase())) {
                                    filteredAirports.add(row);
                                    break;
                                }
                            }
                        }
                    }

                    filteredList = filteredAirports;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<Airport>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView textLocation;
        public TextView textAirport;

        public ViewHolder(View view) {
            super(view);
            textLocation = (TextView) view.findViewById(R.id.text_location);
            textAirport = (TextView) view.findViewById(R.id.text_airport);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Airport airport = filteredList.get(getAdapterPosition());
            airport.setType(type);

            EventRxBus.getInstance().pushEvent(airport);
            ((Activity) context).finish();
        }
    }
}
