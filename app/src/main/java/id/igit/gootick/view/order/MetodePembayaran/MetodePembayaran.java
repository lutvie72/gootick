package id.igit.gootick.view.order.MetodePembayaran;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.view.order.DetailOrderFlight;
import id.igit.gootick.view.order.DetailOrderHotel;
import id.igit.gootick.view.order.DetailOrderKereta;
import id.igit.gootick.view.order.DetailPayment.DetailPaymentDefault;
import id.igit.gootick.view.order.DetailPayment.DetailPaymentTransfer;

public class MetodePembayaran extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.card_transfer)
    CardView cardViewTransfer;
    @BindView(R.id.card_atm)
    CardView cardViewAtm;
    @BindView(R.id.card_bcaklik)
    CardView cardViewBca;
    @BindView(R.id.card_cimb)
    CardView cardViewCimb;
    @BindView(R.id.card_mandiri)
    CardView cardViewMandiri;

    @BindView(R.id.text_detail_pesana)
    TextView textViewDetailPesan;

    String ket = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metode_pembayaran);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText("Metode Pembayaran");

        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);


        ket = getIntent().getStringExtra("ket");

        cardViewAtm.setOnClickListener(this);
        cardViewBca.setOnClickListener(this);
        cardViewCimb.setOnClickListener(this);
        cardViewMandiri.setOnClickListener(this);
        cardViewTransfer.setOnClickListener(this);
        textViewDetailPesan.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.card_bcaklik:
                Intent intentBca = new Intent(this, DetailPaymentDefault.class);
                intentBca.putExtra("jenis","bca");
                startActivity(intentBca);
                break;

            case R.id.card_atm:
                Intent intentAtm = new Intent(this, DetailPaymentDefault.class);
                intentAtm.putExtra("jenis","atm");
                startActivity(intentAtm);
                break;

            case R.id.card_cimb:
                Intent intentCimb = new Intent(this, DetailPaymentDefault.class);
                intentCimb.putExtra("jenis","cimb");
                startActivity(intentCimb);
                break;

            case R.id.card_mandiri:
                Intent intentMandiri = new Intent(this, DetailPaymentDefault.class);
                intentMandiri.putExtra("jenis","mandiri");
                startActivity(intentMandiri);
                break;

            case R.id.card_transfer:
                Intent intentTransfer = new Intent(this, DetailPaymentTransfer.class);
                intentTransfer.putExtra("jenis","transfer");
                startActivity(intentTransfer);
                break;

            case  R.id.text_detail_pesana:
                Intent intent;
                if (ket.equalsIgnoreCase("pesawat")){
                    intent = new Intent(this, DetailOrderFlight.class);
                    startActivity(intent);
                }else if(ket.equalsIgnoreCase("kereta")){
                    intent = new Intent(this, DetailOrderKereta.class);
                    startActivity(intent);
                }else if(ket.equalsIgnoreCase("hotel")){
                     intent = new Intent(this, DetailOrderHotel.class);
                    startActivity(intent);
                }
        }
    }
}
