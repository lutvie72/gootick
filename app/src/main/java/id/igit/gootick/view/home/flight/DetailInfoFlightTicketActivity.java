package id.igit.gootick.view.home.flight;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.entity.airplanedata.gotick.detail.Departures;
import id.igit.gootick.entity.airplanedata.gotick.detail.DetailFlight;
import id.igit.gootick.helper.ViewHelper;
import id.igit.gootick.view.BaseView;

public class DetailInfoFlightTicketActivity extends AppCompatActivity implements DetailInfoFlightTicketView {

    @BindView(R.id.text_title)
    TextView textViewTitle;
    @BindView(R.id.text_class)
    TextView textViewClass;
    @BindView(R.id.text_from)
    TextView textViewFrom;
    @BindView(R.id.text_from_location)
    TextView textViewFromLocation;
    @BindView(R.id.text_duration)
    TextView textViewDuration;
    @BindView(R.id.text_to)
    TextView textViewTo;
    @BindView(R.id.text_to_location)
    TextView textViewToLocation;
    @BindView(R.id.text_time_start)
    TextView textViewTimeStart;
    @BindView(R.id.text_time_end)
    TextView textViewTimeEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_info_flight_ticket);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText(R.string.detail_information);
        textViewTitle.setTextSize(20);
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    @Override
    public void showProgress() {
        ViewHelper.getInstance().showProgressDialog(this);
    }

    @Override
    public void removeProgress() {
        ViewHelper.getInstance().dismisProgressDialog();
    }

    @Override
    public void onLoadError(String error) {
        ViewHelper.showMessage(this, error);
    }

    @Override
    public void onLoadSuccess(DetailFlight result) {
        Departures departures = result.getData().getDepartures();
        textViewTitle.setText(departures.getAirlinesName().toString());
        textViewFrom.setText(departures.getArrivalAirportName());
        textViewFromLocation.setText(departures.getArrivalCity());
        textViewTo.setText(departures.getDepartureAirportName());
        textViewToLocation.setText(departures.getDepartureCity());
        textViewDuration.setText(departures.getDuration());
        textViewTimeStart.setText(departures.getArrivalFlightDate());
        textViewTimeEnd.setText(departures.getDepartureFlightDate());
    }
}
