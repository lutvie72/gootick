package id.igit.gootick.view.home.train;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.helper.ViewHelper;
import id.igit.gootick.view.home.FormPassengerActivity;
import id.igit.gootick.view.order.MetodePembayaran.MetodePembayaran;

public class DetailTrainTicketActivity extends AppCompatActivity implements View.OnClickListener{

    @BindView(R.id.cv1)
    CardView cardView1;
    @BindView(R.id.cv2)
    CardView cardView2;
    @BindView(R.id.edit_title)
    EditText editTextTitle;
    @BindView(R.id.button_continue)
    Button buttonContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_train_ticket);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText(getString(R.string.detail_pasengger));
        textViewTitle.setTextSize(20);
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        cardView1.setOnClickListener(this);
        cardView2.setOnClickListener(this);
        buttonContinue.setOnClickListener(this);
        editTextTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewHelper.showDialogTitleAdult(DetailTrainTicketActivity.this, new ViewHelper.OnCallbackDialogTitle() {
                    @Override
                    public void onSelected(String value) {
                        editTextTitle.setText(value);
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == cardView1.getId()){
            Intent intent = new Intent(this, FormPassengerActivity.class);
            intent.putExtra("type", "adult");
            startActivity(intent);
        }

        if (view.getId() == cardView2.getId()){
            Intent intent = new Intent(this, FormPassengerActivity.class);
            intent.putExtra("type", "child");
            startActivity(intent);
        }
        if(view.getId() == buttonContinue.getId()){
            Intent intent = new Intent(this, MetodePembayaran.class);
            intent.putExtra("ket","kereta");
            startActivity(intent);
        }
    }
}
