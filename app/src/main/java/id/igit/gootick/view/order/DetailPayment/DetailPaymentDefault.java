package id.igit.gootick.view.order.DetailPayment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;

public class DetailPaymentDefault extends AppCompatActivity {

    @BindView(R.id.image_logo_bank)
    ImageView imageViewLogoBank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_payment_default);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        textViewTitle.setText("Detail Payment");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);


        String ketBank = getIntent().getStringExtra("jenis");
        setImageBank(ketBank);
    }

    private void setImageBank(String ketBank) {
        if(ketBank.equalsIgnoreCase("transfer")){
            imageViewLogoBank.setImageDrawable(getResources().getDrawable(R.drawable.transfer));
        }else if(ketBank.equalsIgnoreCase("bca")){
            imageViewLogoBank.setImageDrawable(getResources().getDrawable(R.drawable.bca));
        }else if(ketBank.equalsIgnoreCase("atm")){
            imageViewLogoBank.setImageDrawable(getResources().getDrawable(R.drawable.atm));
        }else if(ketBank.equalsIgnoreCase("cimb")){
            imageViewLogoBank.setImageDrawable(getResources().getDrawable(R.drawable.cimb));
        }else if(ketBank.equalsIgnoreCase("mandiri")){
            imageViewLogoBank.setImageDrawable(getResources().getDrawable(R.drawable.mandiri));
        }
    }
}
