package id.igit.gootick.view;

/**
 * Created by Kristiawan on 07/04/18.
 */

public interface BaseView<T> {

    void showProgress();

    void removeProgress();

    void onLoadError(String error);

    void onLoadSuccess(T result);
}

