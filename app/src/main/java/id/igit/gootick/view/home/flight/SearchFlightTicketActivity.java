package id.igit.gootick.view.home.flight;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.App;
import id.igit.gootick.R;
import id.igit.gootick.entity.airplanedata.SearchQueries;
import id.igit.gootick.entity.airplanedata.airport.Airport;
import id.igit.gootick.entity.airplanedata.airport.gootick.Datum;
import id.igit.gootick.helper.ViewHelper;
import id.igit.gootick.util.EventRxBus;
import id.igit.gootick.helper.GeneraleHelper;
import id.igit.gootick.util.LocalStorage;
import id.igit.gootick.view.CalendarActivity;
import id.igit.gootick.view.home.FormGuestActivity;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class SearchFlightTicketActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.switch_button)
    SwitchCompat switchButton;
    @BindView(R.id.button_search)
    Button buttonSearch;
    @BindView(R.id.edit_from)
    TextView textViewFrom;
    @BindView(R.id.edit_to)
    TextView textViewTo;
    @BindView(R.id.edit_go)
    TextView textViewGo;
    @BindView(R.id.edit_return)
    TextView textViewReturn;
    @BindView(R.id.edit_passenger)
    TextView textViewPassenger;
    @BindView(R.id.edit_class)
    TextView textViewClass;
    @BindView(R.id.layout_return)
    RelativeLayout layoutReturn;
    @BindView(R.id.image_change)
    ImageView imageViewCange;

    private LocalStorage localStorage;

    private SearchQueries searchQueries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_airplane_ticket);
        ButterKnife.bind(this);

        localStorage = new LocalStorage(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText(getIntent().getStringExtra("title"));
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        textViewTitle.setTextSize(20);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        switchButton.setOnCheckedChangeListener(this);
        imageViewCange.setOnClickListener(this);
        buttonSearch.setOnClickListener(this);
        textViewFrom.setOnClickListener(this);
        textViewTo.setOnClickListener(this);
        textViewGo.setOnClickListener(this);
        textViewReturn.setOnClickListener(this);
        textViewPassenger.setOnClickListener(this);
        textViewClass.setOnClickListener(this);

        textViewPassenger.setText("1 "+getString(R.string.adult)+" 0 "+getString(R.string.children)+" 0 "+getString(R.string.infant));
        textViewClass.setText("Ekonomi");

        searchQueries = new SearchQueries();
        searchQueries.setAdult(1);
        searchQueries.setChild(0);
        searchQueries.setInfant(0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventRxBus.getInstance().getEvents().subscribe(new Observer<Object>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onNext(Object value) {
                if (value instanceof ArrayList) {
                    List<Date> dates = (List<Date>) value;

                    if (dates.size() == 1) {
                        textViewGo.setText(GeneraleHelper.convertDate(dates.get(0), GeneraleHelper.SHORT_TEXT_DATE_FORMAT));
                        searchQueries.setDate(GeneraleHelper.convertDate(dates.get(0), GeneraleHelper.DEFAULT_DATE_FORMAT));
                    } else {
                        textViewReturn.setText(GeneraleHelper.convertDate(dates.get(1), GeneraleHelper.SHORT_TEXT_DATE_FORMAT));
                        searchQueries.setRetDate(GeneraleHelper.convertDate(dates.get(1), GeneraleHelper.DEFAULT_DATE_FORMAT));
                    }
                } else if (value instanceof HashMap) {
                    Map<String, Integer> map = (Map<String, Integer>) value;
                    textViewPassenger.setText(map.get("adults")+" " + getString(R.string.adult) +" "+ map.get("children") +" "+ getString(R.string.children) + map.get("infants")+" " + getString(R.string.infant));

                    searchQueries.setAdult(map.get("adults"));
                    searchQueries.setChild(map.get("children"));
                    searchQueries.setInfant(map.get("infants"));
                } else if (value instanceof Airport) {
                    Airport airport = (Airport) value;
                    if (airport.getType().equals("from")) {
                        searchQueries.setFrom(airport.getAirportCode());
                        searchQueries.setmFrom(airport.getAirportName() + "(" + airport.getAirportCode() + ")");
                        textViewFrom.setText(airport.getAirportCode() + " - " + airport.getAirportName());
                    } else {
                        searchQueries.setTo(airport.getAirportCode());
                        searchQueries.setmTo(airport.getAirportName() + "(" + airport.getAirportCode() + ")");
                        textViewTo.setText(airport.getAirportCode() + " - " + airport.getAirportName());
                    }
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == buttonSearch.getId()) {
            searchQueries.setCurrency(localStorage.getCurrency());
            searchQueries.setLanguage(localStorage.getLanguage().toUpperCase());
            if (validation()) {
                Intent intent = new Intent(this, FlightTicketActivity.class);
                intent.putExtra("param", App.getInstance().toJSON(searchQueries));
                startActivity(intent);
            }
        }

        if (view.getId() == textViewFrom.getId()) {
            Intent intent = new Intent(this, SearchFilterActivity.class);
            intent.putExtra("type", "from");
            startActivity(intent);
        }

        if (view.getId() == textViewTo.getId()) {
            Intent intent = new Intent(this, SearchFilterActivity.class);
            intent.putExtra("type", "to");
            startActivity(intent);
        }

        if (view.getId() == textViewGo.getId()) {
            Intent intent = new Intent(this, CalendarActivity.class);
            intent.putExtra("mode", "start");
            startActivity(intent);
        }

        if (view.getId() == textViewReturn.getId()) {
            Intent intent = new Intent(this, CalendarActivity.class);
            intent.putExtra("mode", "end");
            startActivity(intent);
        }

        if (view.getId() == textViewPassenger.getId()) {
            Intent intent = new Intent(this, FormGuestActivity.class);
            startActivity(intent);
        }

        if (view.getId() == imageViewCange.getId()) {
            String temp = textViewFrom.getText().toString();
            textViewFrom.setText(textViewTo.getText());
            textViewTo.setText(temp);

            searchQueries.setTo(textViewTo.getText().toString());
            searchQueries.setFrom(textViewFrom.getText().toString());
        }

        if (view.getId() == textViewClass.getId()) {
            ViewHelper.showDialogClass(this, new ViewHelper.OnCallbackDialogTitle() {
                @Override
                public void onSelected(String value) {
                    textViewClass.setText(value);
                }
            });
        }
    }

    private boolean validation() {
//        long days = GeneraleHelper.countDays(
//                GeneraleHelper.convertStringToDate(searchQueries.getDate()),
//                GeneraleHelper.convertStringToDate(searchQueries.getRetDate()));

        if ((searchQueries == null) ||
                searchQueries.getFrom() == null ||
                searchQueries.getTo() == null ||
                searchQueries.getDate() == null ||
                searchQueries.getAdult() == null) {

            ViewHelper.showMessage(this, getString(R.string.message_field_empty));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            layoutReturn.setVisibility(View.VISIBLE);
            textViewReturn.setText("");
            textViewReturn.setHint(getString(R.string.date));
        } else {
            layoutReturn.setVisibility(View.GONE);
            searchQueries.setRetDate(null);
        }
    }
}
