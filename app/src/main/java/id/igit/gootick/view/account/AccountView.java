package id.igit.gootick.view.account;

import id.igit.gootick.entity.account.User;
import id.igit.gootick.view.BaseView;

/**
 * Created by Kristiawan on 07/09/18.
 */
public interface AccountView extends BaseView<User> {
}
