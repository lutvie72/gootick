package id.igit.gootick.view.home.train;

import android.os.Handler;
import android.os.Looper;

import id.igit.gootick.entity.airplanedata.airport.BaseAirport;
import id.igit.gootick.entity.traindata.gootick.stations.TrainStation;
import id.igit.gootick.service.BaseApiService;
import id.igit.gootick.view.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Kristiawan on 29/04/18.
 */
public class SearchFilterPresenter implements BasePresenter<BaseAirport> {

    private SearchFilterView view;
    private BaseApiService apiService;

    public SearchFilterPresenter(SearchFilterView view) {
        this.view = view;
        apiService = BaseApiService.newInstance();
    }

    @Override
    public void find(BaseAirport param) {
        view.showProgress();
        apiService.getTrainService().findStation()
                .enqueue(new Callback<TrainStation>() {
                    @Override
                    public void onResponse(Call<TrainStation> call, Response<TrainStation> response) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            if (response.isSuccessful()) {
                                view.onLoadSuccess(response.body());
                            }

                            view.removeProgress();
                        });
                    }

                    @Override
                    public void onFailure(Call<TrainStation> call, Throwable t) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            view.onLoadError(t.getMessage());
                            view.removeProgress();
                        });
                    }
                });
    }

    @Override
    public void findById(BaseAirport param) {

    }
}
