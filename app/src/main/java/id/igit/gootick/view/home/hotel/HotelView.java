package id.igit.gootick.view.home.hotel;

import id.igit.gootick.entity.hoteldata.HotelTicket;
import id.igit.gootick.entity.hoteldata.gootick.HotelTiket;
import id.igit.gootick.view.BaseView;

/**
 * Created by Kristiawan on 17/04/18.
 */

public interface HotelView extends BaseView<HotelTiket> {
}
