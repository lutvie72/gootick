package id.igit.gootick.view.account;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.entity.account.Data;
import id.igit.gootick.entity.account.User;
import id.igit.gootick.helper.ViewHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment implements AccountView {

    @BindView(R.id.text_name)
    TextView textViewName;
    @BindView(R.id.text_email)
    TextView textViewEmail;
    @BindView(R.id.text_mobile)
    TextView textViewMobile;
    @BindView(R.id.button_edit)
    Button buttonEditAccount;

    private AccountPresenter presenter;

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this, view);
        presenter = new AccountPresenter(this);
        presenter.findById(new Data("153579e29f0252d08c70ebd4fd30550b"));

        buttonEditAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), EditAccountActivity.class));
            }
        });
        return view;
    }

    @Override
    public void showProgress() {
        ViewHelper.getInstance().showProgressDialog(getContext());
    }

    @Override
    public void removeProgress() {
        ViewHelper.getInstance().dismisProgressDialog();
    }

    @Override
    public void onLoadError(String error) {
        ViewHelper.showMessage(getContext(), error);
    }

    @Override
    public void onLoadSuccess(User result) {
        if (result.getData() != null) {
            textViewName.setText(result.getData().getName());
            textViewEmail.setText(result.getData().getEmail());
            textViewMobile.setText(result.getData().getMobile());
        }
    }
}
