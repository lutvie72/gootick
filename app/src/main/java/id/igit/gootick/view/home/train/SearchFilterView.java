package id.igit.gootick.view.home.train;


import id.igit.gootick.entity.traindata.gootick.stations.TrainStation;
import id.igit.gootick.view.BaseView;

/**
 * Created by Kristiawan on 29/04/18.
 */
public interface SearchFilterView extends BaseView<TrainStation> {
}
