package id.igit.gootick.view.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.entity.hoteldata.Result;
import id.igit.gootick.entity.hoteldata.gootick.Datum;
import id.igit.gootick.util.LocalStorage;
import id.igit.gootick.view.home.flight.DetailFlightTicketActivity;
import id.igit.gootick.view.home.hotel.DetailHotelTicketActivity;
import id.igit.gootick.view.home.hotel.InfoHotelActivity;

/**
 * Created by Kristiawan on 17/04/18.
 */

public class HotelTicketAdapter extends RecyclerView.Adapter<HotelTicketAdapter.TicketHolder> {

    Context context;
    List<Datum> resultList;
    LocalStorage localStorage;

    public HotelTicketAdapter(Context context, List<Datum> resultList) {
        this.context = context;
        this.resultList = resultList;
        localStorage = new LocalStorage(context);
    }

    @Override
    public TicketHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TicketHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_ticket_hotel, parent, false));
    }

    @Override
    public void onBindViewHolder(TicketHolder holder, int position) {
        Datum result = resultList.get(position);
        holder.textViewTitle.setText(result.getName());
        holder.textViewPrice.setText(localStorage.getCurrency().toUpperCase() + " " + result.getPrice());
        holder.textViewLocation.setText(result.getProvinceName());
    }

    @Override
    public int getItemCount() {
        return resultList.size();
    }


    class TicketHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.text_title)
        TextView textViewTitle;
        @BindView(R.id.text_price)
        TextView textViewPrice;
        @BindView(R.id.text_time)
        TextView textViewLocation;

        public TicketHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, InfoHotelActivity.class);
            context.startActivity(intent);
        }
    }
}
