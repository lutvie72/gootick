package id.igit.gootick.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.entity.currency.Result;
import id.igit.gootick.util.LocalStorage;

/**
 * Created by Kristiawan on 17/08/18.
 */
public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.ViewHolder> {

    private List<Result> results;
    private Context context;
    private int lastPosition;
    private LocalStorage localStorage;

    public CurrencyAdapter(List<Result> results, Context context) {
        this.results = results;
        this.context = context;
        localStorage = new LocalStorage(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_curency, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Result currency = results.get(position);
        holder.radioButton.setText(currency.getName());
        holder.radioButton.setChecked(lastPosition == position);

        if (position == 0){
            holder.radioButton.setChecked(false);
        }

        if (currency.getCode().toUpperCase().equals(localStorage.getCurrency().toUpperCase())) {
            holder.radioButton.setChecked(true);
        }
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.radio_curency)
        RadioButton radioButton;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastPosition = getAdapterPosition();
                    localStorage.setCurrency(results.get(lastPosition).getCode().toUpperCase());
                    notifyDataSetChanged();
                }
            });
        }
    }
}
