package id.igit.gootick.view.home.hotel;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.entity.airplanedata.airport.Airport;
import id.igit.gootick.entity.airplanedata.airport.BaseAirport;
import id.igit.gootick.entity.hoteldata.ResultSearchHotel;
import id.igit.gootick.entity.hoteldata.Results;
import id.igit.gootick.entity.hoteldata.SearchHotel;
import id.igit.gootick.entity.hoteldata.gootick.search.HotelSearch;
import id.igit.gootick.entity.hoteldata.gootick.search.Result;
import id.igit.gootick.helper.ViewHelper;
import id.igit.gootick.view.home.SimpleDividerItemDecoration;
import id.igit.gootick.view.home.adapter.SearchHotelAutocompleteAdapter;
import id.igit.gootick.view.home.adapter.SearchRecyclerAdapter;

public class SearchAutocompleteActivity extends AppCompatActivity implements SearchAutocompleteView {

    @BindView(R.id.recycler_autocomplete)
    RecyclerView recyclerView;
    @BindView(R.id.layout_progress)
    RelativeLayout layoutProgress;
    @BindView(R.id.text_info)
    TextView textViewInfo;

    private List<Result> searchHotels;
    private SearchHotelAutocompleteAdapter adapter;
    private SearchAutocompletePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_search);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        EditText editTextSearch = toolbar.findViewById(R.id.search);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        textViewInfo.setVisibility(View.VISIBLE);
        presenter = new SearchAutocompletePresenter(this);
        searchHotels = new ArrayList<>();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        adapter = new SearchHotelAutocompleteAdapter(searchHotels, this);
        recyclerView.setAdapter(adapter);
        editTextSearch.setHint(R.string.search_hotel_or_area);
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textViewInfo.setVisibility(View.GONE);
                presenter.find(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void showProgress() {
        layoutProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeProgress() {
        layoutProgress.setVisibility(View.GONE);
    }

    @Override
    public void onLoadError(String error) {
//        ViewHelper.showMessage(this, error);
    }

    @Override
    public void onLoadSuccess(HotelSearch result) {
        searchHotels.clear();
        if (result.getResults().getResult() != null) {
            searchHotels.addAll(result.getResults().getResult());
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }
}
