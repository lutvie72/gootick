package id.igit.gootick.view.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.common.imageslider.FragmentSlider;
import id.igit.gootick.common.imageslider.SliderIndicator;
import id.igit.gootick.common.imageslider.SliderPagerAdapter;
import id.igit.gootick.entity.Content;
import id.igit.gootick.entity.airplanedata.airport.Airport;
import id.igit.gootick.entity.hoteldata.Result;
import id.igit.gootick.view.home.adapter.HomeAdapter;
import id.igit.gootick.view.home.flight.SearchFlightTicketActivity;
import id.igit.gootick.view.home.hotel.SearchHotelTicketActivity;
import id.igit.gootick.view.home.train.SearchTrainTicketActivity;

public class HomeFragment extends Fragment implements View.OnClickListener, HomeView {

    @BindView(R.id.sliderView)
    ViewPager viewPager;
    @BindView(R.id.pagesContainer)
    LinearLayout linearLayout;
    @BindView(R.id.cardFlight)
    CardView cardViewFlight;
    @BindView(R.id.cardHotel)
    CardView cardViewHotel;
    @BindView(R.id.cardTrain)
    CardView cardViewTrain;
    @BindView(R.id.recycler_popular_airport)
    RecyclerView recyclerViewAirport;
    @BindView(R.id.recycler_promo_hotel)
    RecyclerView recyclerViewHotel;

    private SliderPagerAdapter mAdapter;
    private SliderIndicator mIndicator;
    private HomePresenter presenter;

    private List<Content> contentAirports;
    private List<Content> contentHotels;

    public HomeFragment() {

    }


    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        cardViewFlight.setOnClickListener(this);
        cardViewHotel.setOnClickListener(this);
        cardViewTrain.setOnClickListener(this);

        contentAirports = new ArrayList<>();
        contentHotels = new ArrayList<>();

        setupSlider();
        presenter = new HomePresenter(this);
        presenter.findHotelPromo();
        presenter.findAirportPopular();

        recyclerViewAirport.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewAirport.setAdapter(new HomeAdapter(contentAirports));

        recyclerViewHotel.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewHotel.setAdapter(new HomeAdapter(contentHotels));
        return view;
    }

    private void setupSlider() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(FragmentSlider.newInstance("https://cdn01.tiket.photos/img/business/p/r/business-promo_kereta-20_kejutan_ekstra2018_all_channel_update_landing_page_978x484px.jpg"));
        fragments.add(FragmentSlider.newInstance("https://cdn01.tiket.photos/img/business/p/r/business-promo_free_shuttle_aug_landing_page_978x484px.jpg"));
        fragments.add(FragmentSlider.newInstance("https://cdn01.tiket.photos/img/business/t/e/business-template_lama_promo_spesial_garuda_agustus_landing_page_978x484px.jpg"));
        fragments.add(FragmentSlider.newInstance("https://cdn01.tiket.photos/img/business/p/e/business-pesawat-periode-2_landing_page_978x484px2.jpg"));
//        fragments.add(FragmentSlider.newInstance("https://dummyimage.com/600x290/2daeff/fff"));

        mAdapter = new SliderPagerAdapter(getActivity().getSupportFragmentManager(), fragments);
        viewPager.setAdapter(mAdapter);

        mIndicator = new SliderIndicator(getContext(), linearLayout, viewPager, R.drawable.indicator_circle);
        mIndicator.setPageCount(fragments.size());
        mIndicator.show();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == cardViewFlight.getId()) {
            Intent intent = new Intent(getContext(), SearchFlightTicketActivity.class);
            intent.putExtra("title", getString(R.string.flight));
            startActivity(intent);
        }

        if (view.getId() == cardViewHotel.getId()) {
            Intent intent = new Intent(getContext(), SearchHotelTicketActivity.class);
            intent.putExtra("title", getString(R.string.hotel));
            startActivity(intent);
        }

        if (view.getId() == cardViewTrain.getId()) {
            Intent intent = new Intent(getContext(), SearchTrainTicketActivity.class);
            intent.putExtra("title", getString(R.string.train));
            startActivity(intent);
        }
    }

    @Override
    public void onLoadImageSliders(List<String> images) {

    }

    @Override
    public void onLoadAirportPopular(List<Airport> airports) {
        contentAirports.clear();
        for (Airport airport : airports) {
            Content content = new Content();
            content.setId(1);
            content.setTitle(airport.getBusinessName());
            contentAirports.add(content);
        }

        recyclerViewAirport.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onLoadHotelPromo(List<Result> hotels) {
        contentHotels.clear();
        for (Result hotel : hotels) {
            Content content = new Content();
            content.setId(Integer.valueOf(hotel.getId()));
            content.setTitle(hotel.getName());
            content.setImage(hotel.getPhotoPrimary());
            contentHotels.add(content);
        }

        Content content = new Content();
        content.setTitle("Hotel Santika");
        contentHotels.add(content);
        Content content2 = new Content();
        content.setTitle("Hotel Santika");
        contentHotels.add(content);
        Content content3 = new Content();
        content.setTitle("Hotel Santika");
        contentHotels.add(content);

        recyclerViewHotel.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void removeProgress() {

    }

    @Override
    public void onLoadError() {

    }
}
