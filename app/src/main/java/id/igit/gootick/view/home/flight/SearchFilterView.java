package id.igit.gootick.view.home.flight;

import id.igit.gootick.entity.airplanedata.airport.BaseAirport;
import id.igit.gootick.entity.airplanedata.airport.gootick.Airport;
import id.igit.gootick.view.BaseView;

/**
 * Created by Kristiawan on 29/04/18.
 */
public interface SearchFilterView extends BaseView<BaseAirport> {
}
