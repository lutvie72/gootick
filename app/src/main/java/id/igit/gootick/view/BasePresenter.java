package id.igit.gootick.view;

/**
 * Created by Kristiawan on 07/04/18.
 */

public interface BasePresenter<T> {

    void find(T param);

    void findById(T param);
}
