package id.igit.gootick.view.home.train;

import android.os.Handler;
import android.os.Looper;

import id.igit.gootick.entity.traindata.SearchQueries;
import id.igit.gootick.entity.traindata.TrainTicket;
import id.igit.gootick.entity.traindata.gootick.TrainTiket;
import id.igit.gootick.service.BaseApiService;
import id.igit.gootick.view.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Kristiawan on 17/05/18.
 */
public class TrainTicketPresenter implements BasePresenter<SearchQueries> {

    private TrainTicketView view;
    private BaseApiService apiService;

    public TrainTicketPresenter(TrainTicketView view) {
        this.view = view;
        this.apiService = BaseApiService.newInstance();
    }

    @Override
    public void find(SearchQueries param) {
        view.showProgress();
        apiService.getTrainService().find(param)
                .enqueue(new Callback<TrainTiket>() {
                    @Override
                    public void onResponse(Call<TrainTiket> call, Response<TrainTiket> response) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    view.onLoadSuccess(response.body());
                                }

                                view.removeProgress();
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<TrainTiket> call, Throwable t) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                view.onLoadError(t.getMessage());
                                view.removeProgress();
                            }
                        });
                    }
                });
    }

    @Override
    public void findById(SearchQueries param) {

    }
}
