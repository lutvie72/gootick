package id.igit.gootick.view.home.flight;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.App;
import id.igit.gootick.R;
import id.igit.gootick.helper.ViewHelper;
import id.igit.gootick.util.EventRxBus;
import id.igit.gootick.view.home.FormPassengerActivity;
import id.igit.gootick.view.home.train.DetailTrainTicketActivity;
import id.igit.gootick.view.order.MetodePembayaran.MetodePembayaran;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class DetailFlightTicketActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.button_continue)
    Button buttonContinue;
    @BindView(R.id.edit_title)
    EditText editTextTitle;
    @BindView(R.id.edit_name)
    EditText editTextName;
    @BindView(R.id.edit_email)
    EditText editTextEmail;
    @BindView(R.id.edit_phone)
    EditText editTextPhone;
    @BindView(R.id.layout_passenger)
    LinearLayout layoutPasenger;

    Map<String, String> params = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_flight_ticket);
        ButterKnife.bind(this);

        params.put("flight_id", ""+getIntent().getStringExtra("flight_id"));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.text_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText(getString(R.string.detail_pasengger));
        textViewTitle.setTextSize(20);
        textViewTitle.setTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        buttonContinue.setOnClickListener(this);
        editTextTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewHelper.showDialogTitleAdult(DetailFlightTicketActivity.this, new ViewHelper.OnCallbackDialogTitle() {
                    @Override
                    public void onSelected(String value) {
                        editTextTitle.setText(value);
                    }
                });
            }
        });

        for (int i = 1; i <= 2; i++) {
            int no = i;
            CardView cardView = (CardView) getLayoutInflater().inflate(R.layout.card_pasenger, null);
            TextView textView = cardView.findViewById(R.id.text_passenger);
            textView.setText("Adult " + i);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(DetailFlightTicketActivity.this, FormPassengerActivity.class);
                    intent.putExtra("type", "adult");
                    intent.putExtra("number", no);
                    intent.putExtra("data", App.getInstance().toJSON(params));
                    startActivity(intent);
                }
            });

            layoutPasenger.addView(cardView);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventRxBus.getInstance().getEvents().subscribe(new Observer<Object>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Object value) {
                if (value instanceof Map) {
                    params = (Map<String, String>) value;
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == buttonContinue.getId()) {
            params.put("child", "1");
            params.put("adult", "1");
            params.put("infant", "1");
            params.put("conSalutation", "mr");
            params.put("conFirstName", "adi");
            params.put("conLastName", "wijaya");
            params.put("conPhone", "+6288568686900");
            params.put("conEmailAddress", "adi@gmail.com");

            Intent intent = new Intent(this, MetodePembayaran.class);
            intent.putExtra("ket", "pesawat");
            startActivity(intent);
        }
    }
}
