package id.igit.gootick.view.home.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.igit.gootick.R;
import id.igit.gootick.entity.hoteldata.ResultSearchHotel;
import id.igit.gootick.entity.hoteldata.gootick.search.Result;
import id.igit.gootick.util.EventRxBus;

/**
 * Created by Kristiawan on 16/05/18.
 */
public class SearchHotelAutocompleteAdapter extends RecyclerView.Adapter<SearchHotelAutocompleteAdapter.ViewHolder> {

    private List<Result> searchHotels;
    private Context context;

    public SearchHotelAutocompleteAdapter(List<Result> searchHotels, Context context) {
        this.searchHotels = searchHotels;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_search_autocomplete, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Result hotel = searchHotels.get(position);
        holder.textViewHotel.setText(hotel.getValue());
        holder.textViewLocation.setText(hotel.getLabelLocation());
    }

    @Override
    public int getItemCount() {
        return searchHotels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.text_hotel)
        TextView textViewHotel;
        @BindView(R.id.text_location)
        TextView textViewLocation;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            ((Activity) context).finish();
            EventRxBus.getInstance().pushEvent(searchHotels.get(getAdapterPosition()));
        }
    }
}
