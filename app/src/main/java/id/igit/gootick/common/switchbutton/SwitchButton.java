package id.igit.gootick.common.switchbutton;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import id.igit.gootick.R;

/**
 * Created by Kristiawan on 21/03/18.
 */

public class SwitchButton extends RelativeLayout {

    protected Button buttonLeft;
    protected Button buttonRight;
    private OnCallbackChangedListener listener;
    private Context context;

    public SwitchButton(Context context) {
        super(context);
        init(context, null);
    }

    public SwitchButton(Context context, @NonNull AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public SwitchButton(Context context, @NonNull AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.switch_button_layout, this);
        buttonLeft = findViewById(R.id.button_left);
        buttonRight = findViewById(R.id.button_right);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            buttonLeft.setStateListAnimator(null);
            buttonRight.setStateListAnimator(null);
        }

        this.context = context;
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SwitchButton);
            try {
                if (typedArray.hasValue(R.styleable.SwitchButton_textLeft)) {
                    buttonLeft.setText(typedArray.getString(R.styleable.SwitchButton_textLeft));
                }

                if (typedArray.hasValue(R.styleable.SwitchButton_textRight)) {
                    buttonRight.setText(typedArray.getString(R.styleable.SwitchButton_textRight));
                }

                if (typedArray.hasValue(R.styleable.SwitchButton_textColorLeft)) {
                    buttonLeft.setTextColor(typedArray.getColor(R.styleable.SwitchButton_textColorLeft, 0));
                }

                if (typedArray.hasValue(R.styleable.SwitchButton_textColorRight)) {
                    buttonRight.setTextColor(typedArray.getColor(R.styleable.SwitchButton_textColorRight, 0));
                }
            } finally {
                typedArray.recycle();
            }
        }
    }

    public void setOnCallbackListener(OnCallbackChangedListener listener) {
        this.listener = listener;
        buttonLeft.setOnClickListener(view -> {
            buttonLeft.setBackground(ContextCompat.getDrawable(context, R.drawable.switch_button_presed));
            buttonLeft.setTextColor(ContextCompat.getColor(context, R.color.white));
            buttonRight.setBackground(ContextCompat.getDrawable(context, R.drawable.switch_button_default));
            buttonRight.setTextColor(ContextCompat.getColor(context, R.color.gray_line));
            buttonLeft.bringToFront();
            invalidate();

            listener.onChange(SwitchPosition.LEFT);
        });

        buttonRight.setOnClickListener(view -> {
            buttonRight.setBackground(ContextCompat.getDrawable(context, R.drawable.switch_button_presed));
            buttonRight.setTextColor(ContextCompat.getColor(context, R.color.white));
            buttonLeft.setBackground(ContextCompat.getDrawable(context, R.drawable.switch_button_default));
            buttonLeft.setTextColor(ContextCompat.getColor(context, R.color.gray_line));
            buttonRight.bringToFront();
            invalidate();

            listener.onChange(SwitchPosition.RIGHT);
        });
    }
}
