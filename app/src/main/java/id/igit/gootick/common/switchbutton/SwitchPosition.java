package id.igit.gootick.common.switchbutton;

/**
 * Created by Kristiawan on 21/03/18.
 */

public enum SwitchPosition {
    LEFT, RIGHT
}
