package id.igit.gootick.util;

import id.igit.gootick.BuildConfig;
import id.igit.gootick.service.BaseApi;

/**
 * Created by Kristiawan on 06/03/18.
 */

public class ApiUtil {

    public static String TOKEN = "6ddc075a74ef82da67c000bb97a2903e46a33d66";
    public static int VERSION = 3;
    public static String OUTPUT = "json";
    public static String BASE_API;
    public static String BACKEND_API;
    public static String FLIGHT_API;
    public static String FLIGHT_DETAIL_API;
    public static String FLIGHT_AIRPORT_API;
    public static String FLIGHT_AIRPORT_POPULAR_API;
    public static String FLIGHT_ADD_ORDER_API;
    public static String TRAIN_API;
    public static String TRAIN_STATION_API;
    public static String HOTEL_API;
    public static String HOTEL_PROMO_API;
    public static String HOTEL_SEARCH_API;
    public static String CURRENCY_API;
    public static String REGISTER_API;
    public static String LOGIN_API;
    public static String ACOOUNT_API;

    public ApiUtil() {
    }

    public static void buildApi() {
        BASE_API = BuildConfig.BASE_API;
        BACKEND_API = BuildConfig.BACKEND_API;

        FLIGHT_API = BACKEND_API.concat("search/flight");
        FLIGHT_AIRPORT_API = BASE_API.concat("flight_api/all_airport");
//        FLIGHT_AIRPORT_API = BACKEND_API.concat("general/airport-list");
        FLIGHT_AIRPORT_POPULAR_API = BASE_API.concat("flight_api/getPopularDestination");
        FLIGHT_DETAIL_API = BACKEND_API.concat("detail/flight");
        FLIGHT_ADD_ORDER_API = BASE_API.concat("order/add/flight");

        TRAIN_API = BACKEND_API.concat("search/train");
//        TRAIN_STATION_API = BASE_API.concat("train_api/train_station");
        TRAIN_STATION_API = BACKEND_API.concat("general/station-list");

        HOTEL_API = BACKEND_API.concat("search/hotel");
        HOTEL_PROMO_API = BASE_API.concat("home/hotelDeals");
        HOTEL_SEARCH_API = BACKEND_API.concat("search/autocomplete/hotel");

        CURRENCY_API = BASE_API.concat("general_api/listCurrency");

        REGISTER_API = BACKEND_API.concat("register");
        LOGIN_API = BACKEND_API.concat("login");

        ACOOUNT_API = BACKEND_API.concat("customers");
    }
}
