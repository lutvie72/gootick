package id.igit.gootick.util;

import java.util.Random;

/**
 * Created by Kristiawan on 05/09/18.
 */
public class ColorUtil {

    private static String[] colors = {
            "#DEB887",
            "#FF00FF",
            "#F4A460",
            "#B8860B",
            "#DC143C",
            "#6495ED",
            "#87CEFA",
            "#008080",
            "#FA8072",
            "#FFDEAD",
            "#D2691E",
            "#A52A2A",
            "#3CB371",
            "#3CB371",
            "#BC8F8F",
            "#808000",
            "#A0522D",
            "#4169E1",
            "#9966CC",
            "#F4A460",
            "#808080",
            "#696969",
            "#DC143C",
            "#7FFFD4"};

    public static String randomColor() {
        Random rand = new Random();
        int NumberOfAnswers = colors.length;
        int pick = rand.nextInt(NumberOfAnswers);
        return colors[pick];
    }
}
