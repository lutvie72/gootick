package id.igit.gootick.util;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by sukenda on 6/4/17.
 * https://lorentzos.com/rxjava-as-event-bus-the-right-way-10a36bdd49ba
 */

public class EventRxBus {

    private static EventRxBus instance;

    private PublishSubject<Object> subject = PublishSubject.create();

    public static EventRxBus getInstance() {
        if (instance == null) {
            instance = new EventRxBus();
        }

        return instance;
    }

    /**
     * Pass any event down to event listeners.
     */
    public void pushEvent(Object object) {
        subject.onNext(object);
    }

    /**
     * Subscribe to this Observable. On event, do something
     * e.g. replace a fragment
     */
    public Observable<Object> getEvents() {
        return subject;
    }
}
