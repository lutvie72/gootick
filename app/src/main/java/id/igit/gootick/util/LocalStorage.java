package id.igit.gootick.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import java.util.Locale;

public class LocalStorage {

    public static final String SP_NAME = "gootick";
    public static final String SP_LANGUAGE = "language";
    public static final String SP_CURRENCY = "currency";

    SharedPreferences sp;
    SharedPreferences.Editor spEditor;
    Context context;

    public LocalStorage(Context context) {
        sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
        spEditor = sp.edit();
        this.context = context;
    }

    public void init() {
        spEditor.putString(SP_LANGUAGE, Locale.ENGLISH.getLanguage());
        spEditor.putString(SP_CURRENCY, "USD");
        spEditor.commit();
    }

    public boolean isFound() {
        if (getLanguage().equals("") || getCurrency().equals("")) {
            return false;
        } else {
            return true;
        }
    }

    private Locale getCurrentLocale(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return context.getResources().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            return context.getResources().getConfiguration().locale;
        }
    }

    public String getLanguage() {
        return sp.getString(SP_LANGUAGE, "");
    }

    public void setLanguage(String value) {
        spEditor.putString(SP_LANGUAGE, value);
        spEditor.commit();
    }

    public void setCurrency(String value) {
        spEditor.putString(SP_CURRENCY, value);
        spEditor.commit();
    }

    public String getCurrency() {
        return sp.getString(SP_CURRENCY, "");
    }
}
