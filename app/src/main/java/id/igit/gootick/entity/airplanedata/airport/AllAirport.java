
package id.igit.gootick.entity.airplanedata.airport;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllAirport {

    @SerializedName("airport")
    @Expose
    private List<Airport> airport = null;

    public List<Airport> getAirport() {
        return airport;
    }

    public void setAirport(List<Airport> airport) {
        this.airport = airport;
    }

}
