
package id.igit.gootick.entity.airplanedata.gotick;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("round_trip")
    @Expose
    private Boolean roundTrip;
    @SerializedName("param_query")
    @Expose
    private ParamQuery paramQuery;
    @SerializedName("detail_query")
    @Expose
    private DetailQuery detailQuery;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getRoundTrip() {
        return roundTrip;
    }

    public void setRoundTrip(Boolean roundTrip) {
        this.roundTrip = roundTrip;
    }

    public ParamQuery getParamQuery() {
        return paramQuery;
    }

    public void setParamQuery(ParamQuery paramQuery) {
        this.paramQuery = paramQuery;
    }

    public DetailQuery getDetailQuery() {
        return detailQuery;
    }

    public void setDetailQuery(DetailQuery detailQuery) {
        this.detailQuery = detailQuery;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

}
