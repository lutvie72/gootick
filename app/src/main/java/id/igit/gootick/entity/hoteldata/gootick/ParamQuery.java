
package id.igit.gootick.entity.hoteldata.gootick;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ParamQuery {

    @SerializedName("q")
    @Expose
    private String q;
    @SerializedName("startdate")
    @Expose
    private String startdate;
    @SerializedName("night")
    @Expose
    private String night;
    @SerializedName("enddate")
    @Expose
    private String enddate;
    @SerializedName("room")
    @Expose
    private String room;
    @SerializedName("adult")
    @Expose
    private String adult;
    @SerializedName("child")
    @Expose
    private String child;
    @SerializedName("minprice")
    @Expose
    private Integer minprice;
    @SerializedName("maxprice")
    @Expose
    private Integer maxprice;
    @SerializedName("minstar")
    @Expose
    private Integer minstar;
    @SerializedName("maxstar")
    @Expose
    private Integer maxstar;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longtitude")
    @Expose
    private String longtitude;
    @SerializedName("page")
    @Expose
    private Integer page;

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getNight() {
        return night;
    }

    public void setNight(String night) {
        this.night = night;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getAdult() {
        return adult;
    }

    public void setAdult(String adult) {
        this.adult = adult;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public Integer getMinprice() {
        return minprice;
    }

    public void setMinprice(Integer minprice) {
        this.minprice = minprice;
    }

    public Integer getMaxprice() {
        return maxprice;
    }

    public void setMaxprice(Integer maxprice) {
        this.maxprice = maxprice;
    }

    public Integer getMinstar() {
        return minstar;
    }

    public void setMinstar(Integer minstar) {
        this.minstar = minstar;
    }

    public Integer getMaxstar() {
        return maxstar;
    }

    public void setMaxstar(Integer maxstar) {
        this.maxstar = maxstar;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

}
