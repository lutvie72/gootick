
package id.igit.gootick.entity.hoteldata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultSearches {

    @SerializedName("result")
    @Expose
    private List<ResultSearchHotel> result = null;

    public List<ResultSearchHotel> getResult() {
        return result;
    }

    public void setResult(List<ResultSearchHotel> result) {
        this.result = result;
    }

}
