
package id.igit.gootick.entity.airplanedata.gotick;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailQuery {

    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("detail_dep")
    @Expose
    private DetailDep detailDep;
    @SerializedName("detail_arr")
    @Expose
    private DetailArr detailArr;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("full_date")
    @Expose
    private String fullDate;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public DetailDep getDetailDep() {
        return detailDep;
    }

    public void setDetailDep(DetailDep detailDep) {
        this.detailDep = detailDep;
    }

    public DetailArr getDetailArr() {
        return detailArr;
    }

    public void setDetailArr(DetailArr detailArr) {
        this.detailArr = detailArr;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFullDate() {
        return fullDate;
    }

    public void setFullDate(String fullDate) {
        this.fullDate = fullDate;
    }

}
