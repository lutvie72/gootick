
package id.igit.gootick.entity.traindata.station;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrainStation {

    @SerializedName("output_type")
    @Expose
    private String outputType;
    @SerializedName("diagnostic")
    @Expose
    private Diagnostic diagnostic;
    @SerializedName("stations")
    @Expose
    private Stations stations;
    @SerializedName("token")
    @Expose
    private String token;

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public Diagnostic getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic diagnostic) {
        this.diagnostic = diagnostic;
    }

    public Stations getStations() {
        return stations;
    }

    public void setStations(Stations stations) {
        this.stations = stations;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
