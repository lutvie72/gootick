
package id.igit.gootick.entity.airplanedata.airport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Airport {

    private int id;
    @SerializedName("airport_name")
    @Expose
    private String airportName;
    @SerializedName("airport_code")
    @Expose
    private String airportCode;
    @SerializedName("location_name")
    @Expose
    private String locationName;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("arrival_city")
    @Expose
    private String arrivalCity;
    @SerializedName("business_name")
    @Expose
    private String businessName;
    @SerializedName("province_name")
    @Expose
    private String provinceName;
    private String type;

    public Airport() {
    }

    public Airport(int id, String locationName, String businessName) {
        this.id = id;
        this.locationName = locationName;
        this.businessName = businessName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
