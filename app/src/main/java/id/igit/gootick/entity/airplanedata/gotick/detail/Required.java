
package id.igit.gootick.entity.airplanedata.gotick.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Required {

    @SerializedName("separator")
    @Expose
    private Separator separator;
    @SerializedName("conSalutation")
    @Expose
    private ConSalutation conSalutation;
    @SerializedName("conFirstName")
    @Expose
    private ConFirstName conFirstName;
    @SerializedName("conLastName")
    @Expose
    private ConLastName conLastName;
    @SerializedName("conPhone")
    @Expose
    private ConPhone conPhone;
    @SerializedName("conEmailAddress")
    @Expose
    private ConEmailAddress conEmailAddress;
    @SerializedName("separator_adult1")
    @Expose
    private SeparatorAdult1 separatorAdult1;
    @SerializedName("titlea1")
    @Expose
    private Titlea1 titlea1;
    @SerializedName("firstnamea1")
    @Expose
    private Firstnamea1 firstnamea1;
    @SerializedName("lastnamea1")
    @Expose
    private Lastnamea1 lastnamea1;

    public Separator getSeparator() {
        return separator;
    }

    public void setSeparator(Separator separator) {
        this.separator = separator;
    }

    public ConSalutation getConSalutation() {
        return conSalutation;
    }

    public void setConSalutation(ConSalutation conSalutation) {
        this.conSalutation = conSalutation;
    }

    public ConFirstName getConFirstName() {
        return conFirstName;
    }

    public void setConFirstName(ConFirstName conFirstName) {
        this.conFirstName = conFirstName;
    }

    public ConLastName getConLastName() {
        return conLastName;
    }

    public void setConLastName(ConLastName conLastName) {
        this.conLastName = conLastName;
    }

    public ConPhone getConPhone() {
        return conPhone;
    }

    public void setConPhone(ConPhone conPhone) {
        this.conPhone = conPhone;
    }

    public ConEmailAddress getConEmailAddress() {
        return conEmailAddress;
    }

    public void setConEmailAddress(ConEmailAddress conEmailAddress) {
        this.conEmailAddress = conEmailAddress;
    }

    public SeparatorAdult1 getSeparatorAdult1() {
        return separatorAdult1;
    }

    public void setSeparatorAdult1(SeparatorAdult1 separatorAdult1) {
        this.separatorAdult1 = separatorAdult1;
    }

    public Titlea1 getTitlea1() {
        return titlea1;
    }

    public void setTitlea1(Titlea1 titlea1) {
        this.titlea1 = titlea1;
    }

    public Firstnamea1 getFirstnamea1() {
        return firstnamea1;
    }

    public void setFirstnamea1(Firstnamea1 firstnamea1) {
        this.firstnamea1 = firstnamea1;
    }

    public Lastnamea1 getLastnamea1() {
        return lastnamea1;
    }

    public void setLastnamea1(Lastnamea1 lastnamea1) {
        this.lastnamea1 = lastnamea1;
    }

}
