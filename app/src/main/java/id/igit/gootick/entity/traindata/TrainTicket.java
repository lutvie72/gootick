
package id.igit.gootick.entity.traindata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrainTicket {

    @SerializedName("output_type")
    @Expose
    private String outputType;
    @SerializedName("diagnostic")
    @Expose
    private Diagnostic diagnostic;
    @SerializedName("search_queries")
    @Expose
    private SearchQueries searchQueries;
    @SerializedName("departures")
    @Expose
    private Departures departures;
    @SerializedName("token")
    @Expose
    private String token;

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public Diagnostic getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic diagnostic) {
        this.diagnostic = diagnostic;
    }

    public SearchQueries getSearchQueries() {
        return searchQueries;
    }

    public void setSearchQueries(SearchQueries searchQueries) {
        this.searchQueries = searchQueries;
    }

    public Departures getDepartures() {
        return departures;
    }

    public void setDepartures(Departures departures) {
        this.departures = departures;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
