
package id.igit.gootick.entity.traindata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Diagnostic {

    @SerializedName("elapstime")
    @Expose
    private String elapstime;
    @SerializedName("memoryusage")
    @Expose
    private String memoryusage;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("currency")
    @Expose
    private String currency;

    public String getElapstime() {
        return elapstime;
    }

    public void setElapstime(String elapstime) {
        this.elapstime = elapstime;
    }

    public String getMemoryusage() {
        return memoryusage;
    }

    public void setMemoryusage(String memoryusage) {
        this.memoryusage = memoryusage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
