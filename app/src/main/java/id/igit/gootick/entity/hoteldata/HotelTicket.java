
package id.igit.gootick.entity.hoteldata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HotelTicket {

    @SerializedName("output_type")
    @Expose
    private String outputType;
    @SerializedName("diagnostic")
    @Expose
    private Diagnostic diagnostic;
    @SerializedName("search_queries")
    @Expose
    private SearchQueries searchQueries;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("results")
    @Expose
    private Results results;
    @SerializedName("token")
    @Expose
    private String token;

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public Diagnostic getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic diagnostic) {
        this.diagnostic = diagnostic;
    }

    public SearchQueries getSearchQueries() {
        return searchQueries;
    }

    public void setSearchQueries(SearchQueries searchQueries) {
        this.searchQueries = searchQueries;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }
}
