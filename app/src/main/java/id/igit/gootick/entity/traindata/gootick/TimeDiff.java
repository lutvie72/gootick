
package id.igit.gootick.entity.traindata.gootick;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeDiff {

    @SerializedName("is_next_day")
    @Expose
    private Boolean isNextDay;
    @SerializedName("diff_hour")
    @Expose
    private Integer diffHour;
    @SerializedName("diff_minute")
    @Expose
    private Integer diffMinute;

    public Boolean getIsNextDay() {
        return isNextDay;
    }

    public void setIsNextDay(Boolean isNextDay) {
        this.isNextDay = isNextDay;
    }

    public Integer getDiffHour() {
        return diffHour;
    }

    public void setDiffHour(Integer diffHour) {
        this.diffHour = diffHour;
    }

    public Integer getDiffMinute() {
        return diffMinute;
    }

    public void setDiffMinute(Integer diffMinute) {
        this.diffMinute = diffMinute;
    }

}
