
package id.igit.gootick.entity.airplanedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArrAirport {

    @SerializedName("airport_code")
    @Expose
    private String airportCode;
    @SerializedName("international")
    @Expose
    private String international;
    @SerializedName("trans_name_id")
    @Expose
    private String transNameId;
    @SerializedName("business_name")
    @Expose
    private String businessName;
    @SerializedName("business_name_trans_id")
    @Expose
    private String businessNameTransId;
    @SerializedName("business_id")
    @Expose
    private String businessId;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("province_name")
    @Expose
    private String provinceName;
    @SerializedName("location_name")
    @Expose
    private String locationName;

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getInternational() {
        return international;
    }

    public void setInternational(String international) {
        this.international = international;
    }

    public String getTransNameId() {
        return transNameId;
    }

    public void setTransNameId(String transNameId) {
        this.transNameId = transNameId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessNameTransId() {
        return businessNameTransId;
    }

    public void setBusinessNameTransId(String businessNameTransId) {
        this.businessNameTransId = businessNameTransId;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

}
