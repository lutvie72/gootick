
package id.igit.gootick.entity.traindata.gootick;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("detail_availability")
    @Expose
    private String detailAvailability;
    @SerializedName("train_id")
    @Expose
    private String trainId;
    @SerializedName("train_name")
    @Expose
    private String trainName;
    @SerializedName("departure_station")
    @Expose
    private String departureStation;
    @SerializedName("departure_time")
    @Expose
    private String departureTime;
    @SerializedName("departure_timestamp")
    @Expose
    private Integer departureTimestamp;
    @SerializedName("departure_station_name")
    @Expose
    private String departureStationName;
    @SerializedName("departure_city_name")
    @Expose
    private String departureCityName;
    @SerializedName("arrival_station")
    @Expose
    private String arrivalStation;
    @SerializedName("arrival_time")
    @Expose
    private String arrivalTime;
    @SerializedName("arrival_timestamp")
    @Expose
    private Integer arrivalTimestamp;
    @SerializedName("arrival_station_name")
    @Expose
    private String arrivalStationName;
    @SerializedName("arrival_city_name")
    @Expose
    private String arrivalCityName;
    @SerializedName("class_name")
    @Expose
    private String className;
    @SerializedName("class_name_lang")
    @Expose
    private String classNameLang;
    @SerializedName("detail_class_name")
    @Expose
    private String detailClassName;
    @SerializedName("subclass_name")
    @Expose
    private String subclassName;
    @SerializedName("is_promo")
    @Expose
    private String isPromo;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("time_diff")
    @Expose
    private TimeDiff timeDiff;
    @SerializedName("can_be_shown")
    @Expose
    private Boolean canBeShown;
    @SerializedName("is_closing")
    @Expose
    private Boolean isClosing;
    @SerializedName("formatted_schedule_date")
    @Expose
    private String formattedScheduleDate;
    @SerializedName("price_adult_clean")
    @Expose
    private Integer priceAdultClean;
    @SerializedName("price_adult")
    @Expose
    private String priceAdult;
    @SerializedName("price_child_clean")
    @Expose
    private Integer priceChildClean;
    @SerializedName("price_child")
    @Expose
    private String priceChild;
    @SerializedName("price_infant_clean")
    @Expose
    private Integer priceInfantClean;
    @SerializedName("price_infant")
    @Expose
    private String priceInfant;
    @SerializedName("price_total_clean")
    @Expose
    private Integer priceTotalClean;
    @SerializedName("price_total")
    @Expose
    private String priceTotal;
    @SerializedName("selected_schedule")
    @Expose
    private Boolean selectedSchedule;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("departure_station_string")
    @Expose
    private String departureStationString;
    @SerializedName("arrival_station_string")
    @Expose
    private String arrivalStationString;
    @SerializedName("price_value")
    @Expose
    private Integer priceValue;
    @SerializedName("price_value_format")
    @Expose
    private String priceValueFormat;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDetailAvailability() {
        return detailAvailability;
    }

    public void setDetailAvailability(String detailAvailability) {
        this.detailAvailability = detailAvailability;
    }

    public String getTrainId() {
        return trainId;
    }

    public void setTrainId(String trainId) {
        this.trainId = trainId;
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    public String getDepartureStation() {
        return departureStation;
    }

    public void setDepartureStation(String departureStation) {
        this.departureStation = departureStation;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public Integer getDepartureTimestamp() {
        return departureTimestamp;
    }

    public void setDepartureTimestamp(Integer departureTimestamp) {
        this.departureTimestamp = departureTimestamp;
    }

    public String getDepartureStationName() {
        return departureStationName;
    }

    public void setDepartureStationName(String departureStationName) {
        this.departureStationName = departureStationName;
    }

    public String getDepartureCityName() {
        return departureCityName;
    }

    public void setDepartureCityName(String departureCityName) {
        this.departureCityName = departureCityName;
    }

    public String getArrivalStation() {
        return arrivalStation;
    }

    public void setArrivalStation(String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Integer getArrivalTimestamp() {
        return arrivalTimestamp;
    }

    public void setArrivalTimestamp(Integer arrivalTimestamp) {
        this.arrivalTimestamp = arrivalTimestamp;
    }

    public String getArrivalStationName() {
        return arrivalStationName;
    }

    public void setArrivalStationName(String arrivalStationName) {
        this.arrivalStationName = arrivalStationName;
    }

    public String getArrivalCityName() {
        return arrivalCityName;
    }

    public void setArrivalCityName(String arrivalCityName) {
        this.arrivalCityName = arrivalCityName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassNameLang() {
        return classNameLang;
    }

    public void setClassNameLang(String classNameLang) {
        this.classNameLang = classNameLang;
    }

    public String getDetailClassName() {
        return detailClassName;
    }

    public void setDetailClassName(String detailClassName) {
        this.detailClassName = detailClassName;
    }

    public String getSubclassName() {
        return subclassName;
    }

    public void setSubclassName(String subclassName) {
        this.subclassName = subclassName;
    }

    public String getIsPromo() {
        return isPromo;
    }

    public void setIsPromo(String isPromo) {
        this.isPromo = isPromo;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public TimeDiff getTimeDiff() {
        return timeDiff;
    }

    public void setTimeDiff(TimeDiff timeDiff) {
        this.timeDiff = timeDiff;
    }

    public Boolean getCanBeShown() {
        return canBeShown;
    }

    public void setCanBeShown(Boolean canBeShown) {
        this.canBeShown = canBeShown;
    }

    public Boolean getIsClosing() {
        return isClosing;
    }

    public void setIsClosing(Boolean isClosing) {
        this.isClosing = isClosing;
    }

    public String getFormattedScheduleDate() {
        return formattedScheduleDate;
    }

    public void setFormattedScheduleDate(String formattedScheduleDate) {
        this.formattedScheduleDate = formattedScheduleDate;
    }

    public Integer getPriceAdultClean() {
        return priceAdultClean;
    }

    public void setPriceAdultClean(Integer priceAdultClean) {
        this.priceAdultClean = priceAdultClean;
    }

    public String getPriceAdult() {
        return priceAdult;
    }

    public void setPriceAdult(String priceAdult) {
        this.priceAdult = priceAdult;
    }

    public Integer getPriceChildClean() {
        return priceChildClean;
    }

    public void setPriceChildClean(Integer priceChildClean) {
        this.priceChildClean = priceChildClean;
    }

    public String getPriceChild() {
        return priceChild;
    }

    public void setPriceChild(String priceChild) {
        this.priceChild = priceChild;
    }

    public Integer getPriceInfantClean() {
        return priceInfantClean;
    }

    public void setPriceInfantClean(Integer priceInfantClean) {
        this.priceInfantClean = priceInfantClean;
    }

    public String getPriceInfant() {
        return priceInfant;
    }

    public void setPriceInfant(String priceInfant) {
        this.priceInfant = priceInfant;
    }

    public Integer getPriceTotalClean() {
        return priceTotalClean;
    }

    public void setPriceTotalClean(Integer priceTotalClean) {
        this.priceTotalClean = priceTotalClean;
    }

    public String getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(String priceTotal) {
        this.priceTotal = priceTotal;
    }

    public Boolean getSelectedSchedule() {
        return selectedSchedule;
    }

    public void setSelectedSchedule(Boolean selectedSchedule) {
        this.selectedSchedule = selectedSchedule;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDepartureStationString() {
        return departureStationString;
    }

    public void setDepartureStationString(String departureStationString) {
        this.departureStationString = departureStationString;
    }

    public String getArrivalStationString() {
        return arrivalStationString;
    }

    public void setArrivalStationString(String arrivalStationString) {
        this.arrivalStationString = arrivalStationString;
    }

    public Integer getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(Integer priceValue) {
        this.priceValue = priceValue;
    }

    public String getPriceValueFormat() {
        return priceValueFormat;
    }

    public void setPriceValueFormat(String priceValueFormat) {
        this.priceValueFormat = priceValueFormat;
    }

}
