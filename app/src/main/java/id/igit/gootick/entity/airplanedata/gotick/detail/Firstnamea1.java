
package id.igit.gootick.entity.airplanedata.gotick.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Firstnamea1 {

    @SerializedName("mandatory")
    @Expose
    private Integer mandatory;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("example")
    @Expose
    private String example;
    @SerializedName("FieldText")
    @Expose
    private String fieldText;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("disabled")
    @Expose
    private String disabled;

    public Integer getMandatory() {
        return mandatory;
    }

    public void setMandatory(Integer mandatory) {
        this.mandatory = mandatory;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    public String getFieldText() {
        return fieldText;
    }

    public void setFieldText(String fieldText) {
        this.fieldText = fieldText;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

}
