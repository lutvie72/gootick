
package id.igit.gootick.entity.airplanedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaneTiket {

    @SerializedName("output_type")
    @Expose
    private String outputType;
    @SerializedName("round_trip")
    @Expose
    private Boolean roundTrip;
    @SerializedName("search_queries")
    @Expose
    private SearchQueries searchQueries;
    @SerializedName("go_det")
    @Expose
    private GoDet goDet;
    @SerializedName("ret_det")
    @Expose
    private RetDet retDet;
    @SerializedName("diagnostic")
    @Expose
    private Diagnostic diagnostic;
    @SerializedName("departures")
    @Expose
    private Departures departures;
    @SerializedName("returns")
    @Expose
    private Returns returns;
    @SerializedName("nearby_go_date")
    @Expose
    private NearbyGoDate nearbyGoDate;
    @SerializedName("nearby_ret_date")
    @Expose
    private NearbyRetDate nearbyRetDate;
    @SerializedName("token")
    @Expose
    private String token;

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public Boolean getRoundTrip() {
        return roundTrip;
    }

    public void setRoundTrip(Boolean roundTrip) {
        this.roundTrip = roundTrip;
    }

    public SearchQueries getSearchQueries() {
        return searchQueries;
    }

    public void setSearchQueries(SearchQueries searchQueries) {
        this.searchQueries = searchQueries;
    }

    public GoDet getGoDet() {
        return goDet;
    }

    public void setGoDet(GoDet goDet) {
        this.goDet = goDet;
    }

    public RetDet getRetDet() {
        return retDet;
    }

    public void setRetDet(RetDet retDet) {
        this.retDet = retDet;
    }

    public Diagnostic getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic diagnostic) {
        this.diagnostic = diagnostic;
    }

    public Departures getDepartures() {
        return departures;
    }

    public void setDepartures(Departures departures) {
        this.departures = departures;
    }

    public Returns getReturns() {
        return returns;
    }

    public void setReturns(Returns returns) {
        this.returns = returns;
    }

    public NearbyGoDate getNearbyGoDate() {
        return nearbyGoDate;
    }

    public void setNearbyGoDate(NearbyGoDate nearbyGoDate) {
        this.nearbyGoDate = nearbyGoDate;
    }

    public NearbyRetDate getNearbyRetDate() {
        return nearbyRetDate;
    }

    public void setNearbyRetDate(NearbyRetDate nearbyRetDate) {
        this.nearbyRetDate = nearbyRetDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
