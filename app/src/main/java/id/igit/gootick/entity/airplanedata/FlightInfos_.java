
package id.igit.gootick.entity.airplanedata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightInfos_ {

    @SerializedName("flight_info")
    @Expose
    private List<FlightInfo_> flightInfo = null;

    public List<FlightInfo_> getFlightInfo() {
        return flightInfo;
    }

    public void setFlightInfo(List<FlightInfo_> flightInfo) {
        this.flightInfo = flightInfo;
    }

}
