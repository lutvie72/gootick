
package id.igit.gootick.entity.hoteldata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("business_uri")
    @Expose
    private String businessUri;
    @SerializedName("province_name")
    @Expose
    private String provinceName;
    @SerializedName("kecamatan_name")
    @Expose
    private String kecamatanName;
    @SerializedName("kelurahan_name")
    @Expose
    private String kelurahanName;
    @SerializedName("photo_primary")
    @Expose
    private String photoPrimary;
    @SerializedName("room_facility_name")
    @Expose
    private Object roomFacilityName;
    @SerializedName("wifi")
    @Expose
    private String wifi;
    @SerializedName("promo_name")
    @Expose
    private String promoName;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("regional")
    @Expose
    private String regional;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("name")
    @Expose
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getBusinessUri() {
        return businessUri;
    }

    public void setBusinessUri(String businessUri) {
        this.businessUri = businessUri;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getKecamatanName() {
        return kecamatanName;
    }

    public void setKecamatanName(String kecamatanName) {
        this.kecamatanName = kecamatanName;
    }

    public String getKelurahanName() {
        return kelurahanName;
    }

    public void setKelurahanName(String kelurahanName) {
        this.kelurahanName = kelurahanName;
    }

    public String getPhotoPrimary() {
        return photoPrimary;
    }

    public void setPhotoPrimary(String photoPrimary) {
        this.photoPrimary = photoPrimary;
    }

    public Object getRoomFacilityName() {
        return roomFacilityName;
    }

    public void setRoomFacilityName(Object roomFacilityName) {
        this.roomFacilityName = roomFacilityName;
    }

    public String getWifi() {
        return wifi;
    }

    public void setWifi(String wifi) {
        this.wifi = wifi;
    }

    public String getPromoName() {
        return promoName;
    }

    public void setPromoName(String promoName) {
        this.promoName = promoName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRegional() {
        return regional;
    }

    public void setRegional(String regional) {
        this.regional = regional;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
