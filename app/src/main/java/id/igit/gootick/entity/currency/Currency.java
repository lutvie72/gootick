
package id.igit.gootick.entity.currency;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Currency {

    @SerializedName("diagnostic")
    @Expose
    private Diagnostic diagnostic;
    @SerializedName("output_type")
    @Expose
    private String outputType;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("login_status")
    @Expose
    private String loginStatus;
    @SerializedName("token")
    @Expose
    private String token;

    public Diagnostic getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic diagnostic) {
        this.diagnostic = diagnostic;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
