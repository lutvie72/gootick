
package id.igit.gootick.entity.traindata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("detail_availability")
    @Expose
    private String detailAvailability;
    @SerializedName("schedule_id")
    @Expose
    private String scheduleId;
    @SerializedName("train_id")
    @Expose
    private String trainId;
    @SerializedName("train_name")
    @Expose
    private String trainName;
    @SerializedName("departure_time")
    @Expose
    private String departureTime;
    @SerializedName("arrival_time")
    @Expose
    private String arrivalTime;
    @SerializedName("class_name")
    @Expose
    private String className;
    @SerializedName("subclass_name")
    @Expose
    private String subclassName;
    @SerializedName("is_promo")
    @Expose
    private Integer isPromo;
    @SerializedName("price_adult")
    @Expose
    private String priceAdult;
    @SerializedName("price_child")
    @Expose
    private String priceChild;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("price_infant")
    @Expose
    private String priceInfant;
    @SerializedName("departure_station")
    @Expose
    private String departureStation;
    @SerializedName("arrival_station")
    @Expose
    private String arrivalStation;
    @SerializedName("price_total_clean")
    @Expose
    private int priceTotalClean;
    @SerializedName("class_name_lang")
    @Expose
    private String classNameLang;


    public String getDetailAvailability() {
        return detailAvailability;
    }

    public void setDetailAvailability(String detailAvailability) {
        this.detailAvailability = detailAvailability;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getTrainId() {
        return trainId;
    }

    public void setTrainId(String trainId) {
        this.trainId = trainId;
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSubclassName() {
        return subclassName;
    }

    public void setSubclassName(String subclassName) {
        this.subclassName = subclassName;
    }

    public Integer getIsPromo() {
        return isPromo;
    }

    public void setIsPromo(Integer isPromo) {
        this.isPromo = isPromo;
    }

    public String getPriceAdult() {
        return priceAdult;
    }

    public void setPriceAdult(String priceAdult) {
        this.priceAdult = priceAdult;
    }

    public String getPriceChild() {
        return priceChild;
    }

    public void setPriceChild(String priceChild) {
        this.priceChild = priceChild;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPriceInfant() {
        return priceInfant;
    }

    public void setPriceInfant(String priceInfant) {
        this.priceInfant = priceInfant;
    }

    public String getDepartureStation() {
        return departureStation;
    }

    public void setDepartureStation(String departureStation) {
        this.departureStation = departureStation;
    }

    public String getArrivalStation() {
        return arrivalStation;
    }

    public void setArrivalStation(String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }

    public int getPriceTotalClean() {
        return priceTotalClean;
    }

    public void setPriceTotalClean(int priceTotalClean) {
        this.priceTotalClean = priceTotalClean;
    }

    public String getClassNameLang() {
        return classNameLang;
    }

    public void setClassNameLang(String classNameLang) {
        this.classNameLang = classNameLang;
    }
}
