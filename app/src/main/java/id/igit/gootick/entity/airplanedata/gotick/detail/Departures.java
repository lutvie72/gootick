
package id.igit.gootick.entity.airplanedata.gotick.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Departures {

    @SerializedName("flight_id")
    @Expose
    private String flightId;
    @SerializedName("airlines_name")
    @Expose
    private String airlinesName;
    @SerializedName("flight_number")
    @Expose
    private String flightNumber;
    @SerializedName("flight_date")
    @Expose
    private String flightDate;
    @SerializedName("departure_city")
    @Expose
    private String departureCity;
    @SerializedName("arrival_city")
    @Expose
    private String arrivalCity;
    @SerializedName("stop")
    @Expose
    private String stop;
    @SerializedName("price_value")
    @Expose
    private String priceValue;
    @SerializedName("price_adult")
    @Expose
    private String priceAdult;
    @SerializedName("price_child")
    @Expose
    private String priceChild;
    @SerializedName("price_infant")
    @Expose
    private String priceInfant;
    @SerializedName("count_adult")
    @Expose
    private String countAdult;
    @SerializedName("count_child")
    @Expose
    private String countChild;
    @SerializedName("count_infant")
    @Expose
    private String countInfant;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("check_in_baggage")
    @Expose
    private Integer checkInBaggage;
    @SerializedName("check_in_baggage_unit")
    @Expose
    private String checkInBaggageUnit;
    @SerializedName("simple_departure_time")
    @Expose
    private String simpleDepartureTime;
    @SerializedName("simple_arrival_time")
    @Expose
    private String simpleArrivalTime;
    @SerializedName("long_via")
    @Expose
    private String longVia;
    @SerializedName("departure_city_name")
    @Expose
    private String departureCityName;
    @SerializedName("arrival_city_name")
    @Expose
    private String arrivalCityName;
    @SerializedName("departure_airport_name")
    @Expose
    private String departureAirportName;
    @SerializedName("arrival_airport_name")
    @Expose
    private String arrivalAirportName;
    @SerializedName("full_via")
    @Expose
    private String fullVia;
    @SerializedName("markup_price_string")
    @Expose
    private String markupPriceString;
    @SerializedName("need_baggage")
    @Expose
    private Integer needBaggage;
    @SerializedName("departure_flight_date")
    @Expose
    private String departureFlightDate;
    @SerializedName("departure_flight_date_str")
    @Expose
    private String departureFlightDateStr;
    @SerializedName("departure_flight_date_str_short")
    @Expose
    private String departureFlightDateStrShort;
    @SerializedName("arrival_flight_date")
    @Expose
    private String arrivalFlightDate;
    @SerializedName("arrival_flight_date_str")
    @Expose
    private String arrivalFlightDateStr;
    @SerializedName("arrival_flight_date_str_short")
    @Expose
    private String arrivalFlightDateStrShort;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("flight_infos")
    @Expose
    private FlightInfos flightInfos;
    @SerializedName("sss_key")
    @Expose
    private Object sssKey;
    @SerializedName("price_value_af")
    @Expose
    private String priceValueAf;
    @SerializedName("price_adult_af")
    @Expose
    private String priceAdultAf;
    @SerializedName("price_value_format")
    @Expose
    private String priceValueFormat;
    @SerializedName("price_adult_format")
    @Expose
    private String priceAdultFormat;

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getAirlinesName() {
        return airlinesName;
    }

    public void setAirlinesName(String airlinesName) {
        this.airlinesName = airlinesName;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(String flightDate) {
        this.flightDate = flightDate;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    public String getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(String priceValue) {
        this.priceValue = priceValue;
    }

    public String getPriceAdult() {
        return priceAdult;
    }

    public void setPriceAdult(String priceAdult) {
        this.priceAdult = priceAdult;
    }

    public String getPriceChild() {
        return priceChild;
    }

    public void setPriceChild(String priceChild) {
        this.priceChild = priceChild;
    }

    public String getPriceInfant() {
        return priceInfant;
    }

    public void setPriceInfant(String priceInfant) {
        this.priceInfant = priceInfant;
    }

    public String getCountAdult() {
        return countAdult;
    }

    public void setCountAdult(String countAdult) {
        this.countAdult = countAdult;
    }

    public String getCountChild() {
        return countChild;
    }

    public void setCountChild(String countChild) {
        this.countChild = countChild;
    }

    public String getCountInfant() {
        return countInfant;
    }

    public void setCountInfant(String countInfant) {
        this.countInfant = countInfant;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getCheckInBaggage() {
        return checkInBaggage;
    }

    public void setCheckInBaggage(Integer checkInBaggage) {
        this.checkInBaggage = checkInBaggage;
    }

    public String getCheckInBaggageUnit() {
        return checkInBaggageUnit;
    }

    public void setCheckInBaggageUnit(String checkInBaggageUnit) {
        this.checkInBaggageUnit = checkInBaggageUnit;
    }

    public String getSimpleDepartureTime() {
        return simpleDepartureTime;
    }

    public void setSimpleDepartureTime(String simpleDepartureTime) {
        this.simpleDepartureTime = simpleDepartureTime;
    }

    public String getSimpleArrivalTime() {
        return simpleArrivalTime;
    }

    public void setSimpleArrivalTime(String simpleArrivalTime) {
        this.simpleArrivalTime = simpleArrivalTime;
    }

    public String getLongVia() {
        return longVia;
    }

    public void setLongVia(String longVia) {
        this.longVia = longVia;
    }

    public String getDepartureCityName() {
        return departureCityName;
    }

    public void setDepartureCityName(String departureCityName) {
        this.departureCityName = departureCityName;
    }

    public String getArrivalCityName() {
        return arrivalCityName;
    }

    public void setArrivalCityName(String arrivalCityName) {
        this.arrivalCityName = arrivalCityName;
    }

    public String getDepartureAirportName() {
        return departureAirportName;
    }

    public void setDepartureAirportName(String departureAirportName) {
        this.departureAirportName = departureAirportName;
    }

    public String getArrivalAirportName() {
        return arrivalAirportName;
    }

    public void setArrivalAirportName(String arrivalAirportName) {
        this.arrivalAirportName = arrivalAirportName;
    }

    public String getFullVia() {
        return fullVia;
    }

    public void setFullVia(String fullVia) {
        this.fullVia = fullVia;
    }

    public String getMarkupPriceString() {
        return markupPriceString;
    }

    public void setMarkupPriceString(String markupPriceString) {
        this.markupPriceString = markupPriceString;
    }

    public Integer getNeedBaggage() {
        return needBaggage;
    }

    public void setNeedBaggage(Integer needBaggage) {
        this.needBaggage = needBaggage;
    }

    public String getDepartureFlightDate() {
        return departureFlightDate;
    }

    public void setDepartureFlightDate(String departureFlightDate) {
        this.departureFlightDate = departureFlightDate;
    }

    public String getDepartureFlightDateStr() {
        return departureFlightDateStr;
    }

    public void setDepartureFlightDateStr(String departureFlightDateStr) {
        this.departureFlightDateStr = departureFlightDateStr;
    }

    public String getDepartureFlightDateStrShort() {
        return departureFlightDateStrShort;
    }

    public void setDepartureFlightDateStrShort(String departureFlightDateStrShort) {
        this.departureFlightDateStrShort = departureFlightDateStrShort;
    }

    public String getArrivalFlightDate() {
        return arrivalFlightDate;
    }

    public void setArrivalFlightDate(String arrivalFlightDate) {
        this.arrivalFlightDate = arrivalFlightDate;
    }

    public String getArrivalFlightDateStr() {
        return arrivalFlightDateStr;
    }

    public void setArrivalFlightDateStr(String arrivalFlightDateStr) {
        this.arrivalFlightDateStr = arrivalFlightDateStr;
    }

    public String getArrivalFlightDateStrShort() {
        return arrivalFlightDateStrShort;
    }

    public void setArrivalFlightDateStrShort(String arrivalFlightDateStrShort) {
        this.arrivalFlightDateStrShort = arrivalFlightDateStrShort;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public FlightInfos getFlightInfos() {
        return flightInfos;
    }

    public void setFlightInfos(FlightInfos flightInfos) {
        this.flightInfos = flightInfos;
    }

    public Object getSssKey() {
        return sssKey;
    }

    public void setSssKey(Object sssKey) {
        this.sssKey = sssKey;
    }

    public String getPriceValueAf() {
        return priceValueAf;
    }

    public void setPriceValueAf(String priceValueAf) {
        this.priceValueAf = priceValueAf;
    }

    public String getPriceAdultAf() {
        return priceAdultAf;
    }

    public void setPriceAdultAf(String priceAdultAf) {
        this.priceAdultAf = priceAdultAf;
    }

    public String getPriceValueFormat() {
        return priceValueFormat;
    }

    public void setPriceValueFormat(String priceValueFormat) {
        this.priceValueFormat = priceValueFormat;
    }

    public String getPriceAdultFormat() {
        return priceAdultFormat;
    }

    public void setPriceAdultFormat(String priceAdultFormat) {
        this.priceAdultFormat = priceAdultFormat;
    }

}
