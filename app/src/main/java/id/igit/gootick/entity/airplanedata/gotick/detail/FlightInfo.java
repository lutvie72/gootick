
package id.igit.gootick.entity.airplanedata.gotick.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightInfo {

    @SerializedName("flight_number")
    @Expose
    private String flightNumber;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("departure_city")
    @Expose
    private String departureCity;
    @SerializedName("departure_city_name")
    @Expose
    private String departureCityName;
    @SerializedName("arrival_city")
    @Expose
    private String arrivalCity;
    @SerializedName("arrival_city_name")
    @Expose
    private String arrivalCityName;
    @SerializedName("airlines_name")
    @Expose
    private String airlinesName;
    @SerializedName("departure_date_time")
    @Expose
    private String departureDateTime;
    @SerializedName("string_departure_date")
    @Expose
    private String stringDepartureDate;
    @SerializedName("string_departure_date_short")
    @Expose
    private String stringDepartureDateShort;
    @SerializedName("simple_departure_time")
    @Expose
    private String simpleDepartureTime;
    @SerializedName("arrival_date_time")
    @Expose
    private String arrivalDateTime;
    @SerializedName("string_arrival_date")
    @Expose
    private String stringArrivalDate;
    @SerializedName("string_arrival_date_short")
    @Expose
    private String stringArrivalDateShort;
    @SerializedName("simple_arrival_time")
    @Expose
    private String simpleArrivalTime;
    @SerializedName("img_src")
    @Expose
    private String imgSrc;
    @SerializedName("duration_time")
    @Expose
    private Integer durationTime;
    @SerializedName("duration_hour")
    @Expose
    private String durationHour;
    @SerializedName("duration_minute")
    @Expose
    private String durationMinute;
    @SerializedName("check_in_baggage")
    @Expose
    private Integer checkInBaggage;
    @SerializedName("check_in_baggage_unit")
    @Expose
    private String checkInBaggageUnit;
    @SerializedName("terminal")
    @Expose
    private String terminal;
    @SerializedName("transit_duration_hour")
    @Expose
    private Integer transitDurationHour;
    @SerializedName("transit_duration_minute")
    @Expose
    private Integer transitDurationMinute;
    @SerializedName("transit_arrival_text_city")
    @Expose
    private String transitArrivalTextCity;
    @SerializedName("transit_arrival_text_time")
    @Expose
    private String transitArrivalTextTime;

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getDepartureCityName() {
        return departureCityName;
    }

    public void setDepartureCityName(String departureCityName) {
        this.departureCityName = departureCityName;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getArrivalCityName() {
        return arrivalCityName;
    }

    public void setArrivalCityName(String arrivalCityName) {
        this.arrivalCityName = arrivalCityName;
    }

    public String getAirlinesName() {
        return airlinesName;
    }

    public void setAirlinesName(String airlinesName) {
        this.airlinesName = airlinesName;
    }

    public String getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(String departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public String getStringDepartureDate() {
        return stringDepartureDate;
    }

    public void setStringDepartureDate(String stringDepartureDate) {
        this.stringDepartureDate = stringDepartureDate;
    }

    public String getStringDepartureDateShort() {
        return stringDepartureDateShort;
    }

    public void setStringDepartureDateShort(String stringDepartureDateShort) {
        this.stringDepartureDateShort = stringDepartureDateShort;
    }

    public String getSimpleDepartureTime() {
        return simpleDepartureTime;
    }

    public void setSimpleDepartureTime(String simpleDepartureTime) {
        this.simpleDepartureTime = simpleDepartureTime;
    }

    public String getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(String arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public String getStringArrivalDate() {
        return stringArrivalDate;
    }

    public void setStringArrivalDate(String stringArrivalDate) {
        this.stringArrivalDate = stringArrivalDate;
    }

    public String getStringArrivalDateShort() {
        return stringArrivalDateShort;
    }

    public void setStringArrivalDateShort(String stringArrivalDateShort) {
        this.stringArrivalDateShort = stringArrivalDateShort;
    }

    public String getSimpleArrivalTime() {
        return simpleArrivalTime;
    }

    public void setSimpleArrivalTime(String simpleArrivalTime) {
        this.simpleArrivalTime = simpleArrivalTime;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public Integer getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(Integer durationTime) {
        this.durationTime = durationTime;
    }

    public String getDurationHour() {
        return durationHour;
    }

    public void setDurationHour(String durationHour) {
        this.durationHour = durationHour;
    }

    public String getDurationMinute() {
        return durationMinute;
    }

    public void setDurationMinute(String durationMinute) {
        this.durationMinute = durationMinute;
    }

    public Integer getCheckInBaggage() {
        return checkInBaggage;
    }

    public void setCheckInBaggage(Integer checkInBaggage) {
        this.checkInBaggage = checkInBaggage;
    }

    public String getCheckInBaggageUnit() {
        return checkInBaggageUnit;
    }

    public void setCheckInBaggageUnit(String checkInBaggageUnit) {
        this.checkInBaggageUnit = checkInBaggageUnit;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public Integer getTransitDurationHour() {
        return transitDurationHour;
    }

    public void setTransitDurationHour(Integer transitDurationHour) {
        this.transitDurationHour = transitDurationHour;
    }

    public Integer getTransitDurationMinute() {
        return transitDurationMinute;
    }

    public void setTransitDurationMinute(Integer transitDurationMinute) {
        this.transitDurationMinute = transitDurationMinute;
    }

    public String getTransitArrivalTextCity() {
        return transitArrivalTextCity;
    }

    public void setTransitArrivalTextCity(String transitArrivalTextCity) {
        this.transitArrivalTextCity = transitArrivalTextCity;
    }

    public String getTransitArrivalTextTime() {
        return transitArrivalTextTime;
    }

    public void setTransitArrivalTextTime(String transitArrivalTextTime) {
        this.transitArrivalTextTime = transitArrivalTextTime;
    }

}
