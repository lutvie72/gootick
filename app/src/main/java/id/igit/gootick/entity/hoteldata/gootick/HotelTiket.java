
package id.igit.gootick.entity.hoteldata.gootick;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HotelTiket {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("param_query")
    @Expose
    private ParamQuery paramQuery;
    @SerializedName("detail_query")
    @Expose
    private DetailQuery detailQuery;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("pagination")
    @Expose
    private Pagination pagination;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ParamQuery getParamQuery() {
        return paramQuery;
    }

    public void setParamQuery(ParamQuery paramQuery) {
        this.paramQuery = paramQuery;
    }

    public DetailQuery getDetailQuery() {
        return detailQuery;
    }

    public void setDetailQuery(DetailQuery detailQuery) {
        this.detailQuery = detailQuery;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

}
