
package id.igit.gootick.entity.airplanedata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NearbyGoDate {

    @SerializedName("nearby")
    @Expose
    private List<Nearby> nearby = null;

    public List<Nearby> getNearby() {
        return nearby;
    }

    public void setNearby(List<Nearby> nearby) {
        this.nearby = nearby;
    }

}
