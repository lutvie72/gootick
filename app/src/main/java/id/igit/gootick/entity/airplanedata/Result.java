
package id.igit.gootick.entity.airplanedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("flight_id")
    @Expose
    private String flightId;
    @SerializedName("airlines_name")
    @Expose
    private String airlinesName;
    @SerializedName("flight_number")
    @Expose
    private String flightNumber;
    @SerializedName("price_value")
    @Expose
    private String priceValue;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("price_adult")
    @Expose
    private String priceAdult;
    @SerializedName("price_child")
    @Expose
    private String priceChild;
    @SerializedName("price_infant")
    @Expose
    private String priceInfant;
    @SerializedName("simple_departure_time")
    @Expose
    private String simpleDepartureTime;
    @SerializedName("simple_arrival_time")
    @Expose
    private String simpleArrivalTime;
    @SerializedName("stop")
    @Expose
    private String stop;
    @SerializedName("long_via")
    @Expose
    private String longVia;
    @SerializedName("full_via")
    @Expose
    private String fullVia;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("flight_infos")
    @Expose
    private FlightInfos flightInfos;

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getAirlinesName() {
        return airlinesName;
    }

    public void setAirlinesName(String airlinesName) {
        this.airlinesName = airlinesName;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(String priceValue) {
        this.priceValue = priceValue;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getPriceAdult() {
        return priceAdult;
    }

    public void setPriceAdult(String priceAdult) {
        this.priceAdult = priceAdult;
    }

    public String getPriceChild() {
        return priceChild;
    }

    public void setPriceChild(String priceChild) {
        this.priceChild = priceChild;
    }

    public String getPriceInfant() {
        return priceInfant;
    }

    public void setPriceInfant(String priceInfant) {
        this.priceInfant = priceInfant;
    }

    public String getSimpleDepartureTime() {
        return simpleDepartureTime;
    }

    public void setSimpleDepartureTime(String simpleDepartureTime) {
        this.simpleDepartureTime = simpleDepartureTime;
    }

    public String getSimpleArrivalTime() {
        return simpleArrivalTime;
    }

    public void setSimpleArrivalTime(String simpleArrivalTime) {
        this.simpleArrivalTime = simpleArrivalTime;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    public String getLongVia() {
        return longVia;
    }

    public void setLongVia(String longVia) {
        this.longVia = longVia;
    }

    public String getFullVia() {
        return fullVia;
    }

    public void setFullVia(String fullVia) {
        this.fullVia = fullVia;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public FlightInfos getFlightInfos() {
        return flightInfos;
    }

    public void setFlightInfos(FlightInfos flightInfos) {
        this.flightInfos = flightInfos;
    }

}
