
package id.igit.gootick.entity.airplanedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchQueries {

    @SerializedName("departure")
    @Expose
    private String from = "CGK";
    @SerializedName("arrival")
    @Expose
    private String to = "LHR";
    @SerializedName("date")
    @Expose
    private String date = "2018-11-30";
    @SerializedName("ret_date")
    @Expose
    private String retDate;
    @SerializedName("adult")
    @Expose
    private Integer adult;
    @SerializedName("child")
    @Expose
    private Integer child;
    @SerializedName("infant")
    @Expose
    private Integer infant;

    private String currency;

    private String language;

    private String mFrom;
    private String mTo;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRetDate() {
        return retDate;
    }

    public void setRetDate(String retDate) {
        this.retDate = retDate;
    }

    public Integer getAdult() {
        return adult;
    }

    public void setAdult(Integer adult) {
        this.adult = adult;
    }

    public Integer getChild() {
        return child;
    }

    public void setChild(Integer child) {
        this.child = child;
    }

    public Integer getInfant() {
        return infant;
    }

    public void setInfant(Integer infant) {
        this.infant = infant;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getmFrom() {
        return mFrom;
    }

    public void setmFrom(String mFrom) {
        this.mFrom = mFrom;
    }

    public String getmTo() {
        return mTo;
    }

    public void setmTo(String mTo) {
        this.mTo = mTo;
    }
}
