
package id.igit.gootick.entity.airplanedata.gotick.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("required")
    @Expose
    private Required required;
    @SerializedName("departures")
    @Expose
    private Departures departures;

    public Required getRequired() {
        return required;
    }

    public void setRequired(Required required) {
        this.required = required;
    }

    public Departures getDepartures() {
        return departures;
    }

    public void setDepartures(Departures departures) {
        this.departures = departures;
    }

}
