
package id.igit.gootick.entity.airplanedata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NearbyRetDate {

    @SerializedName("nearby")
    @Expose
    private List<Nearby_> nearby = null;

    public List<Nearby_> getNearby() {
        return nearby;
    }

    public void setNearby(List<Nearby_> nearby) {
        this.nearby = nearby;
    }

}
