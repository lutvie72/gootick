
package id.igit.gootick.entity.airplanedata.gotick;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("flight_id")
    @Expose
    private String flightId;
    @SerializedName("airlines_name")
    @Expose
    private String airlinesName;
    @SerializedName("flight_number")
    @Expose
    private String flightNumber;
    @SerializedName("departure_city")
    @Expose
    private String departureCity;
    @SerializedName("arrival_city")
    @Expose
    private String arrivalCity;
    @SerializedName("transit")
    @Expose
    private String transit;
    @SerializedName("price_value_bef")
    @Expose
    private String priceValueBef;
    @SerializedName("price_adult_bef")
    @Expose
    private String priceAdultBef;
    @SerializedName("price_child_bef")
    @Expose
    private String priceChildBef;
    @SerializedName("price_infant_bef")
    @Expose
    private String priceInfantBef;
    @SerializedName("price_value_af")
    @Expose
    private String priceValueAf;
    @SerializedName("price_adult_af")
    @Expose
    private String priceAdultAf;
    @SerializedName("price_child_af")
    @Expose
    private String priceChildAf;
    @SerializedName("price_infant_af")
    @Expose
    private String priceInfantAf;
    @SerializedName("price_value_format")
    @Expose
    private String priceValueFormat;
    @SerializedName("price_adult_format")
    @Expose
    private String priceAdultFormat;
    @SerializedName("price_child_format")
    @Expose
    private String priceChildFormat;
    @SerializedName("price_infant_format")
    @Expose
    private String priceInfantFormat;
    @SerializedName("price_value_int")
    @Expose
    private Integer priceValueInt;
    @SerializedName("airlines_real_name")
    @Expose
    private String airlinesRealName;
    @SerializedName("airlines_short_real_name")
    @Expose
    private String airlinesShortRealName;
    @SerializedName("has_food")
    @Expose
    private String hasFood;
    @SerializedName("check_in_baggage")
    @Expose
    private String checkInBaggage;
    @SerializedName("show_promo_tag")
    @Expose
    private Boolean showPromoTag;
    @SerializedName("is_promo")
    @Expose
    private Integer isPromo;
    @SerializedName("airport_tax")
    @Expose
    private Boolean airportTax;
    @SerializedName("check_in_baggage_unit")
    @Expose
    private String checkInBaggageUnit;
    @SerializedName("simple_departure_time")
    @Expose
    private String simpleDepartureTime;
    @SerializedName("simple_arrival_time")
    @Expose
    private String simpleArrivalTime;
    @SerializedName("long_via")
    @Expose
    private String longVia;
    @SerializedName("departure_city_name")
    @Expose
    private String departureCityName;
    @SerializedName("arrival_city_name")
    @Expose
    private String arrivalCityName;
    @SerializedName("departure_airport_name")
    @Expose
    private String departureAirportName;
    @SerializedName("arrival_airport_name")
    @Expose
    private String arrivalAirportName;
    @SerializedName("full_via")
    @Expose
    private String fullVia;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("departure_flight_date")
    @Expose
    private String departureFlightDate;
    @SerializedName("departure_flight_date_str")
    @Expose
    private String departureFlightDateStr;
    @SerializedName("departure_flight_date_str_short")
    @Expose
    private String departureFlightDateStrShort;
    @SerializedName("arrival_flight_date")
    @Expose
    private String arrivalFlightDate;
    @SerializedName("arrival_flight_date_str")
    @Expose
    private String arrivalFlightDateStr;
    @SerializedName("arrival_flight_date_str_short")
    @Expose
    private String arrivalFlightDateStrShort;
    @SerializedName("flight_detail")
    @Expose
    private List<FlightDetail> flightDetail = null;

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getAirlinesName() {
        return airlinesName;
    }

    public void setAirlinesName(String airlinesName) {
        this.airlinesName = airlinesName;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getTransit() {
        return transit;
    }

    public void setTransit(String transit) {
        this.transit = transit;
    }

    public String getPriceValueBef() {
        return priceValueBef;
    }

    public void setPriceValueBef(String priceValueBef) {
        this.priceValueBef = priceValueBef;
    }

    public String getPriceAdultBef() {
        return priceAdultBef;
    }

    public void setPriceAdultBef(String priceAdultBef) {
        this.priceAdultBef = priceAdultBef;
    }

    public String getPriceChildBef() {
        return priceChildBef;
    }

    public void setPriceChildBef(String priceChildBef) {
        this.priceChildBef = priceChildBef;
    }

    public String getPriceInfantBef() {
        return priceInfantBef;
    }

    public void setPriceInfantBef(String priceInfantBef) {
        this.priceInfantBef = priceInfantBef;
    }

    public String getPriceValueAf() {
        return priceValueAf;
    }

    public void setPriceValueAf(String priceValueAf) {
        this.priceValueAf = priceValueAf;
    }

    public String getPriceAdultAf() {
        return priceAdultAf;
    }

    public void setPriceAdultAf(String priceAdultAf) {
        this.priceAdultAf = priceAdultAf;
    }

    public String getPriceChildAf() {
        return priceChildAf;
    }

    public void setPriceChildAf(String priceChildAf) {
        this.priceChildAf = priceChildAf;
    }

    public String getPriceInfantAf() {
        return priceInfantAf;
    }

    public void setPriceInfantAf(String priceInfantAf) {
        this.priceInfantAf = priceInfantAf;
    }

    public String getPriceValueFormat() {
        return priceValueFormat;
    }

    public void setPriceValueFormat(String priceValueFormat) {
        this.priceValueFormat = priceValueFormat;
    }

    public String getPriceAdultFormat() {
        return priceAdultFormat;
    }

    public void setPriceAdultFormat(String priceAdultFormat) {
        this.priceAdultFormat = priceAdultFormat;
    }

    public String getPriceChildFormat() {
        return priceChildFormat;
    }

    public void setPriceChildFormat(String priceChildFormat) {
        this.priceChildFormat = priceChildFormat;
    }

    public String getPriceInfantFormat() {
        return priceInfantFormat;
    }

    public void setPriceInfantFormat(String priceInfantFormat) {
        this.priceInfantFormat = priceInfantFormat;
    }

    public Integer getPriceValueInt() {
        return priceValueInt;
    }

    public void setPriceValueInt(Integer priceValueInt) {
        this.priceValueInt = priceValueInt;
    }

    public String getAirlinesRealName() {
        return airlinesRealName;
    }

    public void setAirlinesRealName(String airlinesRealName) {
        this.airlinesRealName = airlinesRealName;
    }

    public String getAirlinesShortRealName() {
        return airlinesShortRealName;
    }

    public void setAirlinesShortRealName(String airlinesShortRealName) {
        this.airlinesShortRealName = airlinesShortRealName;
    }

    public String getHasFood() {
        return hasFood;
    }

    public void setHasFood(String hasFood) {
        this.hasFood = hasFood;
    }

    public String getCheckInBaggage() {
        return checkInBaggage;
    }

    public void setCheckInBaggage(String checkInBaggage) {
        this.checkInBaggage = checkInBaggage;
    }

    public Boolean getShowPromoTag() {
        return showPromoTag;
    }

    public void setShowPromoTag(Boolean showPromoTag) {
        this.showPromoTag = showPromoTag;
    }

    public Integer getIsPromo() {
        return isPromo;
    }

    public void setIsPromo(Integer isPromo) {
        this.isPromo = isPromo;
    }

    public Boolean getAirportTax() {
        return airportTax;
    }

    public void setAirportTax(Boolean airportTax) {
        this.airportTax = airportTax;
    }

    public String getCheckInBaggageUnit() {
        return checkInBaggageUnit;
    }

    public void setCheckInBaggageUnit(String checkInBaggageUnit) {
        this.checkInBaggageUnit = checkInBaggageUnit;
    }

    public String getSimpleDepartureTime() {
        return simpleDepartureTime;
    }

    public void setSimpleDepartureTime(String simpleDepartureTime) {
        this.simpleDepartureTime = simpleDepartureTime;
    }

    public String getSimpleArrivalTime() {
        return simpleArrivalTime;
    }

    public void setSimpleArrivalTime(String simpleArrivalTime) {
        this.simpleArrivalTime = simpleArrivalTime;
    }

    public String getLongVia() {
        return longVia;
    }

    public void setLongVia(String longVia) {
        this.longVia = longVia;
    }

    public String getDepartureCityName() {
        return departureCityName;
    }

    public void setDepartureCityName(String departureCityName) {
        this.departureCityName = departureCityName;
    }

    public String getArrivalCityName() {
        return arrivalCityName;
    }

    public void setArrivalCityName(String arrivalCityName) {
        this.arrivalCityName = arrivalCityName;
    }

    public String getDepartureAirportName() {
        return departureAirportName;
    }

    public void setDepartureAirportName(String departureAirportName) {
        this.departureAirportName = departureAirportName;
    }

    public String getArrivalAirportName() {
        return arrivalAirportName;
    }

    public void setArrivalAirportName(String arrivalAirportName) {
        this.arrivalAirportName = arrivalAirportName;
    }

    public String getFullVia() {
        return fullVia;
    }

    public void setFullVia(String fullVia) {
        this.fullVia = fullVia;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDepartureFlightDate() {
        return departureFlightDate;
    }

    public void setDepartureFlightDate(String departureFlightDate) {
        this.departureFlightDate = departureFlightDate;
    }

    public String getDepartureFlightDateStr() {
        return departureFlightDateStr;
    }

    public void setDepartureFlightDateStr(String departureFlightDateStr) {
        this.departureFlightDateStr = departureFlightDateStr;
    }

    public String getDepartureFlightDateStrShort() {
        return departureFlightDateStrShort;
    }

    public void setDepartureFlightDateStrShort(String departureFlightDateStrShort) {
        this.departureFlightDateStrShort = departureFlightDateStrShort;
    }

    public String getArrivalFlightDate() {
        return arrivalFlightDate;
    }

    public void setArrivalFlightDate(String arrivalFlightDate) {
        this.arrivalFlightDate = arrivalFlightDate;
    }

    public String getArrivalFlightDateStr() {
        return arrivalFlightDateStr;
    }

    public void setArrivalFlightDateStr(String arrivalFlightDateStr) {
        this.arrivalFlightDateStr = arrivalFlightDateStr;
    }

    public String getArrivalFlightDateStrShort() {
        return arrivalFlightDateStrShort;
    }

    public void setArrivalFlightDateStrShort(String arrivalFlightDateStrShort) {
        this.arrivalFlightDateStrShort = arrivalFlightDateStrShort;
    }

    public List<FlightDetail> getFlightDetail() {
        return flightDetail;
    }

    public void setFlightDetail(List<FlightDetail> flightDetail) {
        this.flightDetail = flightDetail;
    }

}
