
package id.igit.gootick.entity.airplanedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GoDet {

    @SerializedName("dep_airport")
    @Expose
    private DepAirport depAirport;
    @SerializedName("arr_airport")
    @Expose
    private ArrAirport arrAirport;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("formatted_date")
    @Expose
    private String formattedDate;

    public DepAirport getDepAirport() {
        return depAirport;
    }

    public void setDepAirport(DepAirport depAirport) {
        this.depAirport = depAirport;
    }

    public ArrAirport getArrAirport() {
        return arrAirport;
    }

    public void setArrAirport(ArrAirport arrAirport) {
        this.arrAirport = arrAirport;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFormattedDate() {
        return formattedDate;
    }

    public void setFormattedDate(String formattedDate) {
        this.formattedDate = formattedDate;
    }

}
