
package id.igit.gootick.entity.hoteldata.gootick.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HotelSearch {

    @SerializedName("diagnostic")
    @Expose
    private Diagnostic diagnostic;
    @SerializedName("output_type")
    @Expose
    private String outputType;
    @SerializedName("results")
    @Expose
    private Results results;
    @SerializedName("login_status")
    @Expose
    private String loginStatus;
    @SerializedName("token")
    @Expose
    private String token;

    public Diagnostic getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic diagnostic) {
        this.diagnostic = diagnostic;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
