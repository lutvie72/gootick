
package id.igit.gootick.entity.airplanedata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Returns {

    @SerializedName("result")
    @Expose
    private List<Result_> result = null;

    public List<Result_> getResult() {
        return result;
    }

    public void setResult(List<Result_> result) {
        this.result = result;
    }

}
