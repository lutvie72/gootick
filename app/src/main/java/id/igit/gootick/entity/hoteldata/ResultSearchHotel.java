
package id.igit.gootick.entity.hoteldata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultSearchHotel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("weight")
    @Expose
    private Double weight;
    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("skey")
    @Expose
    private Double skey;
    @SerializedName("hotel_hit")
    @Expose
    private String hotelHit;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("label_location")
    @Expose
    private String labelLocation;
    @SerializedName("count_location")
    @Expose
    private String countLocation;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("tipe")
    @Expose
    private String tipe;
    @SerializedName("business_uri")
    @Expose
    private String businessUri;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Double getSkey() {
        return skey;
    }

    public void setSkey(Double skey) {
        this.skey = skey;
    }

    public String getHotelHit() {
        return hotelHit;
    }

    public void setHotelHit(String hotelHit) {
        this.hotelHit = hotelHit;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabelLocation() {
        return labelLocation;
    }

    public void setLabelLocation(String labelLocation) {
        this.labelLocation = labelLocation;
    }

    public String getCountLocation() {
        return countLocation;
    }

    public void setCountLocation(String countLocation) {
        this.countLocation = countLocation;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getBusinessUri() {
        return businessUri;
    }

    public void setBusinessUri(String businessUri) {
        this.businessUri = businessUri;
    }

}
