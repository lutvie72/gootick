
package id.igit.gootick.entity.airplanedata.gotick;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ParamQuery {

    @SerializedName("d")
    @Expose
    private String d;
    @SerializedName("a")
    @Expose
    private String a;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("adult")
    @Expose
    private Integer adult;
    @SerializedName("child")
    @Expose
    private Integer child;
    @SerializedName("infant")
    @Expose
    private Integer infant;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("currency")
    @Expose
    private String currency;

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getAdult() {
        return adult;
    }

    public void setAdult(Integer adult) {
        this.adult = adult;
    }

    public Integer getChild() {
        return child;
    }

    public void setChild(Integer child) {
        this.child = child;
    }

    public Integer getInfant() {
        return infant;
    }

    public void setInfant(Integer infant) {
        this.infant = infant;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
