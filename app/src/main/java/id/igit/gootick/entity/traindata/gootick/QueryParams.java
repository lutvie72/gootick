
package id.igit.gootick.entity.traindata.gootick;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QueryParams {

    @SerializedName("dep_station")
    @Expose
    private String depStation;
    @SerializedName("arr_station")
    @Expose
    private String arrStation;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("train_class")
    @Expose
    private String trainClass;
    @SerializedName("count_adult")
    @Expose
    private Integer countAdult;
    @SerializedName("count_child")
    @Expose
    private Integer countChild;
    @SerializedName("count_infant")
    @Expose
    private Integer countInfant;
    @SerializedName("dep_city")
    @Expose
    private String depCity;
    @SerializedName("arr_city")
    @Expose
    private String arrCity;
    @SerializedName("return_date")
    @Expose
    private String returnDate;

    public String getDepStation() {
        return depStation;
    }

    public void setDepStation(String depStation) {
        this.depStation = depStation;
    }

    public String getArrStation() {
        return arrStation;
    }

    public void setArrStation(String arrStation) {
        this.arrStation = arrStation;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTrainClass() {
        return trainClass;
    }

    public void setTrainClass(String trainClass) {
        this.trainClass = trainClass;
    }

    public Integer getCountAdult() {
        return countAdult;
    }

    public void setCountAdult(Integer countAdult) {
        this.countAdult = countAdult;
    }

    public Integer getCountChild() {
        return countChild;
    }

    public void setCountChild(Integer countChild) {
        this.countChild = countChild;
    }

    public Integer getCountInfant() {
        return countInfant;
    }

    public void setCountInfant(Integer countInfant) {
        this.countInfant = countInfant;
    }

    public String getDepCity() {
        return depCity;
    }

    public void setDepCity(String depCity) {
        this.depCity = depCity;
    }

    public String getArrCity() {
        return arrCity;
    }

    public void setArrCity(String arrCity) {
        this.arrCity = arrCity;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

}
