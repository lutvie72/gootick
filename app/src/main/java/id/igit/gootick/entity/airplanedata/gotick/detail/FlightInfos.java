
package id.igit.gootick.entity.airplanedata.gotick.detail;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightInfos {

    @SerializedName("flight_info")
    @Expose
    private List<FlightInfo> flightInfo = null;

    public List<FlightInfo> getFlightInfo() {
        return flightInfo;
    }

    public void setFlightInfo(List<FlightInfo> flightInfo) {
        this.flightInfo = flightInfo;
    }

}
