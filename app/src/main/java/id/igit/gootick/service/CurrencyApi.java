package id.igit.gootick.service;

import id.igit.gootick.entity.currency.Currency;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Kristiawan on 17/08/18.
 */
public interface CurrencyApi {

    @GET
    Call<Currency> find(@Url String url,
                        @Query("token") String token,
                        @Query("output") String output);
}
