package id.igit.gootick.service;

import id.igit.gootick.entity.hoteldata.HotelTicket;
import id.igit.gootick.entity.hoteldata.SearchHotel;
import id.igit.gootick.entity.hoteldata.SearchQueries;
import id.igit.gootick.entity.hoteldata.gootick.HotelTiket;
import id.igit.gootick.entity.hoteldata.gootick.search.HotelSearch;
import id.igit.gootick.util.ApiUtil;
import retrofit2.Call;

/**
 * Created by Kristiawan on 07/04/18.
 */

public class HotelService extends BaseService {

    private static HotelService instance;

    public static HotelService getInstance() {
        if (instance == null) {
            instance = new HotelService();
        }

        return instance;
    }

    //    public Call<HotelTicket> find(SearchQueries searchQueries) {
//        return baseApi.getHotelApi().find(
//                ApiUtil.HOTEL_API,
//                searchQueries.getQ(),
//                searchQueries.getStartdate(),
//                searchQueries.getNight(),
//                searchQueries.getEnddate(),
//                searchQueries.getRoom(),
//                searchQueries.getAdult(),
//                searchQueries.getChild(),
//                ApiUtil.TOKEN,
//                ApiUtil.OUTPUT);
//    }
    public Call<HotelTiket> find(SearchQueries searchQueries) {
        return baseApi.getHotelApi().find(
                ApiUtil.HOTEL_API, searchQueries);
    }

    public Call<HotelTicket> findHotelPromo() {
        return baseApi.getHotelApi().findHotelPromo(
                ApiUtil.HOTEL_PROMO_API,
                ApiUtil.TOKEN,
                ApiUtil.OUTPUT
        );
    }

    //    public Call<HotelSearch> searchHotelAutocomplete(String keyword) {
//        return baseApi.getHotelApi().searchHotelAutocomplete(
//                ApiUtil.HOTEL_SEARCH_API,
//                keyword,
//                ApiUtil.TOKEN,
//                ApiUtil.OUTPUT
//        );
//    }
    public Call<HotelSearch> searchHotelAutocomplete(String keyword) {
        return baseApi.getHotelApi().searchHotelAutocomplete(
                ApiUtil.HOTEL_SEARCH_API, keyword
        );
    }
}
