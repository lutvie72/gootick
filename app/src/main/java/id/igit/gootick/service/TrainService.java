package id.igit.gootick.service;

import id.igit.gootick.entity.traindata.SearchQueries;
import id.igit.gootick.entity.traindata.TrainTicket;
import id.igit.gootick.entity.traindata.gootick.TrainTiket;
import id.igit.gootick.entity.traindata.gootick.stations.TrainStation;
import id.igit.gootick.util.ApiUtil;
import retrofit2.Call;

/**
 * Created by Kristiawan on 07/04/18.
 */

public class TrainService extends BaseService {

    private static TrainService instance;

    public static TrainService getInstance() {
        if (instance == null) {
            instance = new TrainService();
        }

        return instance;
    }

//    public Call<TrainTicket> find(SearchQueries searchQueries) {
//        if (searchQueries.getRetDate().equals(null)) {
//            return baseApi.getTrainApi().find(
//                    ApiUtil.TRAIN_API,
//                    searchQueries.getDepStation(),
//                    searchQueries.getArrStation(),
//                    searchQueries.getDate(),
//                    searchQueries.getAdult(),
//                    searchQueries.getChild(),
//                    searchQueries.getInfant(),
//                    searchQueries.getTrainClass(),
//                    ApiUtil.TOKEN,
//                    ApiUtil.OUTPUT);
//        } else {
//            return baseApi.getTrainApi().find(
//                    ApiUtil.TRAIN_API,
//                    searchQueries.getDepStation(),
//                    searchQueries.getArrStation(),
//                    searchQueries.getDate(),
//                    searchQueries.getRetDate(),
//                    searchQueries.getAdult(),
//                    searchQueries.getChild(),
//                    searchQueries.getInfant(),
//                    searchQueries.getTrainClass(),
//                    ApiUtil.TOKEN,
//                    ApiUtil.OUTPUT);
//        }
//    }

    public Call<TrainTiket> find(SearchQueries searchQueries) {
        return baseApi.getTrainApi().find(ApiUtil.TRAIN_API, searchQueries);
    }

    public Call<TrainStation> findStation() {
        return baseApi.getTrainApi().findStation(
                ApiUtil.TRAIN_STATION_API);
    }
}
