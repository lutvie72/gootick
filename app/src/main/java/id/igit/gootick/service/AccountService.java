package id.igit.gootick.service;

import id.igit.gootick.entity.account.Data;
import id.igit.gootick.entity.account.User;
import id.igit.gootick.util.ApiUtil;
import retrofit2.Call;

/**
 * Created by Kristiawan on 05/09/18.
 */
public class AccountService extends BaseService{

    private static AccountService instance;

    public static AccountService getInstance() {
        if (instance == null) {
            instance = new AccountService();
        }

        return instance;
    }

    public Call<User> register(Data user){
        return baseApi.getAccountApi().register(ApiUtil.REGISTER_API, user);
    }

    public Call<User> login(Data user){
        return baseApi.getAccountApi().login(ApiUtil.LOGIN_API, user);
    }

    public Call<User> getUsers(String uuid){
        return baseApi.getAccountApi().getUser(ApiUtil.ACOOUNT_API.concat("/").concat(uuid));
    }
}
