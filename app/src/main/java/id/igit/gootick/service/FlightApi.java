package id.igit.gootick.service;

import java.util.HashMap;
import java.util.Map;

import id.igit.gootick.entity.airplanedata.SearchQueries;
import id.igit.gootick.entity.airplanedata.airport.BaseAirport;
import id.igit.gootick.entity.airplanedata.airport.gootick.Airport;
import id.igit.gootick.entity.airplanedata.gotick.PlaneTiket;
import id.igit.gootick.entity.airplanedata.gotick.detail.DetailFlight;
import id.igit.gootick.entity.airplanedata.order.FlightOrder;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by Kristiawan on 07/04/18.
 */

public interface FlightApi {

//    @GET
//    Call<PlaneTiket> find(@Url String url,
//                          @Query("d") String from,
//                          @Query("a") String to,
//                          @Query("date") String date,
//                          @Query("ret_date") String returnDate,
//                          @Query("adult") int adult,
//                          @Query("child") int child,
//                          @Query("infant") int intfant,
//                          @Query("v") int version,
//                          @Query("token") String token,
//                          @Query("output") String output);
//
//    @GET
//    Call<PlaneTiket> find(@Url String url,
//                          @Query("d") String from,
//                          @Query("a") String to,
//                          @Query("date") String date,
//                          @Query("adult") int adult,
//                          @Query("child") int child,
//                          @Query("infant") int intfant,
//                          @Query("v") int version,
//                          @Query("token") String token,
//                          @Query("output") String output);

    @POST
    Call<PlaneTiket> find(@Url String url,
                          @Body SearchQueries queries);

    @FormUrlEncoded
    @POST
    Call<DetailFlight> findById(@Url String url,
                                @Field("flight_id") String flightId);

    @GET
    Call<BaseAirport> findAirport(@Url String url,
                              @Query("token") String token,
                              @Query("output") String output);

    @GET
    Call<BaseAirport> findAirportPopular(@Url String url,
                                  @Query("token") String token,
                                  @Query("output") String output);

    @GET
    Call<FlightOrder> addOrder(@Url String url,
                               @QueryMap HashMap<String, String> map,
                               @Query("token") String token,
                               @Query("output") String output);


}
