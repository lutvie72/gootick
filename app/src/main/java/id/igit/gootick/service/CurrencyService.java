package id.igit.gootick.service;

import id.igit.gootick.entity.currency.Currency;
import id.igit.gootick.util.ApiUtil;
import retrofit2.Call;

/**
 * Created by Kristiawan on 17/08/18.
 */
public class CurrencyService extends BaseService {

    private static CurrencyService instance;

    public static CurrencyService getInstance() {
        if (instance == null) {
            instance = new CurrencyService();
        }

        return instance;
    }

    public Call<Currency> find() {
        return baseApi.getCurrencyApi().find(ApiUtil.CURRENCY_API,
                ApiUtil.TOKEN,
                ApiUtil.OUTPUT);
    }
}
