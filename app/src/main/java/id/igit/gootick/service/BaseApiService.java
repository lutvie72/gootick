package id.igit.gootick.service;

/**
 * Created by Kristiawan on 07/03/18.
 */

public class BaseApiService {

    private static class BaseApiServiceHolder {
        private static final BaseApiService INSTANCE = new BaseApiService();
    }

    public static BaseApiService newInstance() {
        return BaseApiServiceHolder.INSTANCE;
    }

    public FlightService getFlightService() {
        return FlightService.getInstance();
    }

    public TrainService getTrainService() {
        return TrainService.getInstance();
    }

    public HotelService getHotelService() {
        return HotelService.getInstance();
    }

    public CurrencyService getCurrencyService() {
        return CurrencyService.getInstance();
    }

    public AccountService getAccountService(){
        return AccountService.getInstance();
    }
}
