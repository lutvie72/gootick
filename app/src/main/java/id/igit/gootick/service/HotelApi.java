package id.igit.gootick.service;

import id.igit.gootick.entity.hoteldata.HotelTicket;
import id.igit.gootick.entity.hoteldata.SearchQueries;
import id.igit.gootick.entity.hoteldata.gootick.HotelTiket;
import id.igit.gootick.entity.hoteldata.gootick.search.HotelSearch;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Kristiawan on 07/04/18.
 */

public interface HotelApi {

    //    @GET
//    Call<HotelTiket> find(@Url String url,
//                          @Query("q") String keyword,
//                          @Query("startdate") String startDate,
//                          @Query("night") String night,
//                          @Query("enddate") String endDate,
//                          @Query("room") String room,
//                          @Query("adult") String adult,
//                          @Query("child") int child,
//                          @Query("token") String token,
//                          @Query("output") String output);
    @POST
    Call<HotelTiket> find(@Url String url, @Body SearchQueries searchQueries);

    @GET
    Call<HotelTicket> findHotelPromo(@Url String url,
                                     @Query("token") String token,
                                     @Query("output") String output);

    //    @GET
//    Call<HotelSearch> searchHotelAutocomplete(@Url String url,
//                                              @Query("q") String keyword,
//                                              @Query("token") String token,
//
//                                      @Query("output") String output);
    @FormUrlEncoded
    @POST
    Call<HotelSearch> searchHotelAutocomplete(@Url String url, @Field("query") String query);
}
