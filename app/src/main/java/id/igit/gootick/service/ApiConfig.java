package id.igit.gootick.service;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import id.igit.gootick.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Kristiawan on 06/03/18.
 */

public class ApiConfig implements BaseApi {

    private Retrofit retrofit;

    private ApiConfig() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_API)
                .callbackExecutor(Executors.newCachedThreadPool())
                .validateEagerly(true)
                .addConverterFactory(GsonConverterFactory.create())
                .client(setupOkHttpClient().build())
                .build();
    }

    private static class ApiConfigHolder {

        private static final ApiConfig INSTANCE = new ApiConfig();
    }

    public static ApiConfig getInstance() {
        return ApiConfigHolder.INSTANCE;
    }

    private OkHttpClient.Builder setupOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.writeTimeout(10, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        }

        httpClient.addInterceptor(logging);
        return httpClient;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    @Override
    public FlightApi getFlightApi() {
        return retrofit.create(FlightApi.class);
    }

    @Override
    public HotelApi getHotelApi() {
        return retrofit.create(HotelApi.class);
    }

    @Override
    public TrainApi getTrainApi() {
        return retrofit.create(TrainApi.class);
    }

    @Override
    public CurrencyApi getCurrencyApi() {
        return retrofit.create(CurrencyApi.class);
    }

    @Override
    public AccountApi getAccountApi() {
        return retrofit.create(AccountApi.class);
    }
}
