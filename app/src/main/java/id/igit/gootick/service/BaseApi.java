package id.igit.gootick.service;

import id.igit.gootick.entity.currency.Currency;

/**
 * Created by Kristiawan on 06/03/18.
 */

public interface BaseApi {

    FlightApi getFlightApi();

    HotelApi getHotelApi();

    TrainApi getTrainApi();

    CurrencyApi getCurrencyApi();

    AccountApi getAccountApi();
}
