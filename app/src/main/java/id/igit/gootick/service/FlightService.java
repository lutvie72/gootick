package id.igit.gootick.service;

import com.google.android.gms.common.api.Api;

import java.util.HashMap;

import id.igit.gootick.entity.airplanedata.SearchQueries;
import id.igit.gootick.entity.airplanedata.airport.BaseAirport;
import id.igit.gootick.entity.airplanedata.airport.gootick.Airport;
import id.igit.gootick.entity.airplanedata.gotick.PlaneTiket;
import id.igit.gootick.entity.airplanedata.gotick.detail.DetailFlight;
import id.igit.gootick.entity.airplanedata.order.FlightOrder;
import id.igit.gootick.util.ApiUtil;
import retrofit2.Call;

/**
 * Created by Kristiawan on 07/04/18.
 */

public class FlightService extends BaseService {

    private static FlightService instance;

    public static FlightService getInstance() {
        if (instance == null) {
            instance = new FlightService();
        }

        return instance;
    }

    public Call<PlaneTiket> find(SearchQueries searchQueries) {
        if (searchQueries.getRetDate() != null) {
//            return baseApi.getFlightApi().find(
//                    ApiUtil.FLIGHT_API,
//                    searchQueries.getFrom(),
//                    searchQueries.getTo(),
//                    searchQueries.getDate(),
//                    searchQueries.getAdult(),
//                    searchQueries.getChild(),
//                    searchQueries.getInfant(),
//                    ApiUtil.VERSION,
//                    ApiUtil.TOKEN,
//                    ApiUtil.OUTPUT);
            return baseApi.getFlightApi().find(
                    ApiUtil.FLIGHT_API,
                    searchQueries);
        } else {
//            return baseApi.getFlightApi().find(
//                    ApiUtil.FLIGHT_API,
//                    searchQueries.getFrom(),
//                    searchQueries.getTo(),
//                    searchQueries.getDate(),
//                    searchQueries.getRetDate(),
//                    searchQueries.getAdult(),
//                    searchQueries.getChild(),
//                    searchQueries.getInfant(),
//                    ApiUtil.VERSION,
//                    ApiUtil.TOKEN,
//                    ApiUtil.OUTPUT);
            return baseApi.getFlightApi().find(
                    ApiUtil.FLIGHT_API,
                    searchQueries);
        }
    }

    public Call<DetailFlight> findById(String flightId) {
        return baseApi.getFlightApi().findById(
                ApiUtil.FLIGHT_DETAIL_API
                , flightId
        );
    }

    public Call<BaseAirport> findAitport() {
        return baseApi.getFlightApi().findAirport(
                ApiUtil.FLIGHT_AIRPORT_API,
                ApiUtil.TOKEN,
                ApiUtil.OUTPUT);
    }

    public Call<BaseAirport> findAitportPopular() {
        return baseApi.getFlightApi().findAirportPopular(
                ApiUtil.FLIGHT_AIRPORT_POPULAR_API,
                ApiUtil.TOKEN,
                ApiUtil.OUTPUT);
    }

    public Call<FlightOrder> addOrder(HashMap<String, String> map) {
        return baseApi.getFlightApi().addOrder(
                ApiUtil.FLIGHT_ADD_ORDER_API,
                map,
                ApiUtil.TOKEN,
                ApiUtil.OUTPUT);
    }
}
