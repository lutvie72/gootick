package id.igit.gootick.service;

import id.igit.gootick.entity.traindata.SearchQueries;
import id.igit.gootick.entity.traindata.gootick.TrainTiket;
import id.igit.gootick.entity.traindata.gootick.stations.TrainStation;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Kristiawan on 07/04/18.
 */

public interface TrainApi {

    //    @GET
//    Call<TrainTicket> find(@Url String url,
//                           @Query("d") String from,
//                           @Query("a") String to,
//                           @Query("date") String date,
//                           @Query("ret_date") String returnDate,
//                           @Query("adult") int adult,
//                           @Query("child") int child,
//                           @Query("infant") int infant,
//                           @Query("class") String classes,
//                           @Query("token") String token,
//                           @Query("output") String output);
    @POST
    Call<TrainTiket> find(@Url String url, @Body SearchQueries queries);

//    @GET
//    Call<TrainTicket> find(@Url String url,
//                           @Query("d") String from,
//                           @Query("a") String to,
//                           @Query("date") String date,
//                           @Query("adult") int adult,
//                           @Query("child") int child,
//                           @Query("infant") int infant,
//                           @Query("class") String classes,
//                           @Query("token") String token,
//                           @Query("output") String output);

//    @GET
//    Call<TrainStation> findStation(@Url String url,
//                                   @Query("token") String token,
//                                   @Query("output") String output);

    @GET
    Call<TrainStation> findStation(@Url String url);
}
