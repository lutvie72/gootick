package id.igit.gootick.service;

import id.igit.gootick.entity.account.Data;
import id.igit.gootick.entity.account.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by Kristiawan on 05/09/18.
 */
public interface AccountApi {

    @POST
    Call<User> register(@Url String url,
                    @Body Data user);

    @POST
    Call<User> login(@Url String url,
                        @Body Data user);

    @GET
    Call<User> getUser(@Url String url);
}
