package id.igit.gootick.helper;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import id.igit.gootick.R;

/**
 * Created by Kristiawan on 12/04/18.
 */

public class ImageHelper {
    private static ImageHelper instance;

    private ImageHelper() {
    }

    public static synchronized ImageHelper getInstance() {
        if (instance == null) {
            return instance = new ImageHelper();
        }

        return instance;
    }

    public void setImageFromUrl(Context context, String url, final ImageView imageView) {
        Picasso.with(context)
                .load(url)
                .error(R.drawable.image)
                .into(imageView);
    }
}
