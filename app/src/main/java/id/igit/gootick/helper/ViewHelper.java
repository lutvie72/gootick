package id.igit.gootick.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import id.igit.gootick.R;
import id.igit.gootick.util.LocalStorage;

/**
 * Created by Kristiawan on 16/05/18.
 */
public class ViewHelper {
    private static ViewHelper instance;
    private ProgressDialog progressDialog;
    private static AppCompatDialog dialog;

    public static ViewHelper getInstance() {
        if (instance == null) {
            instance = new ViewHelper();
        }

        return instance;
    }

    public void showProgressDialog(Context context) {
        progressDialog = ProgressDialog.show(context, "", "Loading. Please wait...", true);
    }

    public void dismisProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public static void showMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showDialogRoom(Context context, final OnCallbackDialogRoom listener) {
        int max = 8;
        final int[] count = {1};
        if (context != null) {
            dialog = new AppCompatDialog(context, R.style.dialogCustomTheme);
            dialog.setContentView(R.layout.dialog_room);
            ImageView imageViewClose = dialog.findViewById(R.id.imageview_close);
            ImageView imageViewMinus = dialog.findViewById(R.id.imageview_minus);
            ImageView imageViewPlus = dialog.findViewById(R.id.imageview_positif);
            TextView textViewCount = dialog.findViewById(R.id.textview_count);
            Button buttonSave = dialog.findViewById(R.id.button_save);
            imageViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            textViewCount.setText("1");

            imageViewPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (count[0] < max) {
                        count[0] += 1;
                        textViewCount.setText(count[0] + "");
                    }
                }
            });

            imageViewMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (count[0] > 1) {
                        count[0] -= 1;
                        textViewCount.setText(count[0] + "");
                    }
                }
            });

            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onSave(count[0]);
                    dialog.dismiss();
                }
            });

            if (!dialog.isShowing()) {
                dialog.show();
            }
        }
    }

    public static void showDialogGuest(Context context, final OnCallbackDialogGuest listener) {
        int max = 16;
        final int[] count = {1};
        if (context != null) {
            dialog = new AppCompatDialog(context, R.style.dialogCustomTheme);
            dialog.setContentView(R.layout.dialog_room);
            ImageView imageViewClose = dialog.findViewById(R.id.imageview_close);
            ImageView imageViewMinus = dialog.findViewById(R.id.imageview_minus);
            ImageView imageViewPlus = dialog.findViewById(R.id.imageview_positif);
            TextView textViewCount = dialog.findViewById(R.id.textview_count);
            Button buttonSave = dialog.findViewById(R.id.button_save);
            imageViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            imageViewPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (count[0] < max) {
                        count[0] += 1;
                        textViewCount.setText(count[0] + "");
                    }
                }
            });

            imageViewMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (count[0] > 1) {
                        count[0] -= 1;
                        textViewCount.setText(count[0] + "");
                    }
                }
            });

            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onSave(count[0]);
                    dialog.dismiss();
                }
            });

            if (!dialog.isShowing()) {
                dialog.show();
            }
        }
    }

    public static void showDialogTitleAdult(Context context, final OnCallbackDialogTitle listener) {
        if (context != null) {
            dialog = new AppCompatDialog(context, R.style.dialogCustomTheme);
            dialog.setContentView(R.layout.dialog_title_adult);
            ImageView imageViewClose = dialog.findViewById(R.id.imageview_close);
            Button buttonSave = dialog.findViewById(R.id.button_ok);
            RadioGroup radioGroup = dialog.findViewById(R.id.group_title);

            imageViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RadioButton radioButton = dialog.findViewById(radioGroup.getCheckedRadioButtonId());
                    listener.onSelected(radioButton.getText().toString());
                    dialog.dismiss();
                }
            });

            if (!dialog.isShowing()) {
                dialog.show();
            }
        }
    }

    public static void showDialogTitleChild(Context context, final OnCallbackDialogTitle listener) {
        if (context != null) {
            dialog = new AppCompatDialog(context, R.style.dialogCustomTheme);
            dialog.setContentView(R.layout.dialog_title_child);
            ImageView imageViewClose = dialog.findViewById(R.id.imageview_close);
            Button buttonSave = dialog.findViewById(R.id.button_ok);
            RadioGroup radioGroup = dialog.findViewById(R.id.group_title);
            imageViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RadioButton radioButton = dialog.findViewById(radioGroup.getCheckedRadioButtonId());
                    listener.onSelected(radioButton.getText().toString());
                    dialog.dismiss();
                }
            });

            if (!dialog.isShowing()) {
                dialog.show();
            }
        }
    }

    public static void showDialogClass(Context context, final OnCallbackDialogTitle listener) {
        if (context != null) {
            dialog = new AppCompatDialog(context, R.style.dialogCustomTheme);
            dialog.setContentView(R.layout.dialog_class_cabin);
            ImageView imageViewClose = dialog.findViewById(R.id.imageview_close);
            Button buttonSave = dialog.findViewById(R.id.button_ok);
            RadioGroup radioGroup = dialog.findViewById(R.id.group_title);
            imageViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RadioButton radioButton = dialog.findViewById(radioGroup.getCheckedRadioButtonId());
                    listener.onSelected(radioButton.getText().toString());
                    dialog.dismiss();
                }
            });

            if (!dialog.isShowing()) {
                dialog.show();
            }
        }
    }

    public static void showDialogLeanguage(LocalStorage localStorage, Context context, final OnCallbackDialogTitle listener) {
        if (context != null) {
            dialog = new AppCompatDialog(context, R.style.dialogCustomTheme);
            dialog.setContentView(R.layout.dialog_languange);
            ImageView imageViewClose = dialog.findViewById(R.id.imageview_close);
            Button buttonSave = dialog.findViewById(R.id.button_ok);
            RadioGroup radioGroup = dialog.findViewById(R.id.group_title);

            RadioButton radioButtonID = radioGroup.findViewById(R.id.radio_id);
            RadioButton radioButtonEN = radioGroup.findViewById(R.id.radio_en);

            if (localStorage.getLanguage().equals("in")){
                radioButtonID.setChecked(true);
            } else {
                radioButtonEN.setChecked(true);
            }

            imageViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RadioButton radioButton = dialog.findViewById(radioGroup.getCheckedRadioButtonId());
                    listener.onSelected(radioButton.getText().toString());
                    dialog.dismiss();
                }
            });

            if (!dialog.isShowing()) {
                dialog.show();
            }
        }
    }

    public interface OnCallbackDialogRoom {
        void onSave(int count);
    }

    public interface OnCallbackDialogGuest {
        void onSave(int count);
    }

    public interface OnCallbackDialogTitle {
        void onSelected(String value);
    }
}
