package id.igit.gootick.helper;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Location;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


/**
 * Created by sukenda on 26/11/17
 * <p>
 * MIT License
 * <p>
 * Copyright (c) 2017 MGS (Mega Giga Solusindo)
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class GeneraleHelper {

//    public static final String DEFAULT_DATE_FORMAT = "dd-MM-yyyy";
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    public static final String LONG_DATE_FORMAT = "dd MMM yy, HH:mm";
    public static final String LONG_TEXT_DATE_FORMAT = "EEEE, dd MMMM yyyy";
    public static final String LONG_DATE_FORMAT_FOR_STRING = "dd-MM-yyyy HH:mm a";
    public static final String SHORT_DATE_FORMAT = "dd MMM yy";
    public static final String SHORT_TEXT_DATE_FORMAT = "dd MMM yyyy";
    public static final String DAY_DATE_FORMAT = "dd";
    public static final String MONTH_DATE_FORMAT = "MMMM";
    public static final String YEAR_DATE_FORMAT = "yyyy";
    private static GeneraleHelper instance;
    private static Context context;

    //private constructor to avoid client applications to use constructor
    private GeneraleHelper(Context context) {
        GeneraleHelper.context = context;
    }

    public static GeneraleHelper getInstance(Context context) {
        if (instance == null) {
            instance = new GeneraleHelper(context);
            return instance;
        } else {
            return instance;
        }
    }


    /**
     * Menghitung hari dari dua tanggal yang berbeda,
     * agak rumit juga
     */
//    public int countDays(String dateOne, String dateTwo) {
//        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat();
//        Date strDate = null;
//        Date endDate = null;
//        try {
//            strDate = formatter.parse(dateOne);//catch exception
//            endDate = formatter.parse(dateTwo);//catch exception
//        } catch (java.text.ParseException e) {
//            PlunqApp.log(this, e.getMessage());
//        }
//
//        if (strDate != null && endDate != null) {
//            long diff = endDate.getTime() - strDate.getTime();
//            long days = diff / (24 * 60 * 60 * 1000);
//
//            if (days != 0) {
//                return (int) days;
//            } else {
//                return 0;
//            }
//        }
//
//        return 0;
//    }





    /**
     * 2017-01-31
     **/
    public static String formatStringDate(String tanggal) {
        Date date = convertStringToDate(tanggal);
        String result;
        if (date != null) {
            result = convertDate(date, LONG_TEXT_DATE_FORMAT);
            if (result != null) {
                return convertDays(result);
            } else {
                return tanggal;
            }
        } else {
            return tanggal;
        }
    }


    /**
     * Convert Hari dari inggris ke Indo
     **/
    public static String convertDays(String date) {
        if (date.contains("Monday")) {
            return date.replace("Monday", "Senin");
        } else if (date.contains("Tuesday")) {
            return date.replace("Tuesday", "Selasa");
        } else if (date.contains("Wednesday")) {
            return date.replace("Wednesday", "Rabu");
        } else if (date.contains("Thursday")) {
            return date.replace("Thursday", "Kamis");
        } else if (date.contains("Friday")) {
            return date.replace("Friday", "Jumat");
        } else if (date.contains("Saturday")) {
            return date.replace("Saturday", "Sabtu");
        } else if (date.contains("Sunday")) {
            return date.replace("Sunday", "Minggu");
        }

        return null;
    }

    public static String getDifftime(Date date) {
        long diff = new Date().getTime() - date.getTime();
        long diffSeconds = TimeUnit.MILLISECONDS.toSeconds(diff);
        long diffMinutes = TimeUnit.MILLISECONDS.toMinutes(diff);
        long diffHours = TimeUnit.MILLISECONDS.toHours(diff);
        long diffDays = TimeUnit.MILLISECONDS.toDays(diff);
        if (diffSeconds >= 60) {
            if (diffMinutes >= 60) {
                if (diffHours >= 24) {
                    return diffDays + " Days ago";
                }

                return diffHours + " Hour ago";
            }

            return diffMinutes + " Minutes ago";
        } else {
            return " Just now";
        }
    }

    public static long countDays(Date date1, Date date2){
        System.out.println("night "+ date1.getTime());
        long diff = date2.getTime() - date1.getTime();
        long diffDays = TimeUnit.MILLISECONDS.toDays(diff);
        return diffDays;
    }

    public static void setLanguage(Context context, String param) {
        Locale myLocale = new Locale(param);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @NonNull
    public static String formattedTime(long milliseconds) {
        return android.text.format.DateFormat.format("dd MMM yyyy - hh:mm a", milliseconds).toString();
    }

    public String getFormattedTime(long timestamp) {
        long oneDayInMillis = 24 * 60 * 60 * 1000;
        long timeDifference = System.currentTimeMillis() - timestamp;
        if (timeDifference < oneDayInMillis) {
            return android.text.format.DateFormat.format("hh:mm a", timestamp).toString();
        } else {
            return android.text.format.DateFormat.format("dd MMM - hh:mm a", timestamp).toString();
        }
    }

    public static String convertDate(Date date, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        return dateFormat.format(date);
    }

    public static String convertDate(Date date, String format, Locale locale) {
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Jakarta");
        DateFormat dateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
        dateFormat.setTimeZone(timeZone);
        return dateFormat.format(date);
    }


    public static Date convertStringToDate(final String string) {
        ThreadLocal threadLocal = new ThreadLocal() {
            protected SimpleDateFormat initialValue() {
                return new SimpleDateFormat(DEFAULT_DATE_FORMAT, Locale.getDefault());
            }
        };

        try {
            return ((SimpleDateFormat) threadLocal.get()).parse(string);
        } catch (ParseException e) {

        }

        return null;
    }

    public static float getTrackDistanceGPXFile(List<Location> locations) {
        float sumDistance = 0;
        for (int i = 0; i < locations.size(); i++) {
            android.location.Location location1 = locations.get(i);
            i++;
            if (i < locations.size()) {
                float[] dist = new float[1];
                android.location.Location.distanceBetween(location1.getLatitude(), location1.getLongitude(), locations.get(i).getLatitude(), locations.get(i).getLongitude(), dist);
                sumDistance = sumDistance + dist[0];
            }
        }

        return sumDistance / 1000;
    }

}
