package id.igit.gootick;

import android.content.res.Configuration;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.Locale;

import id.igit.gootick.util.ApiUtil;
import id.igit.gootick.util.LocalStorage;

/**
 * Created by Kristiawan on 06/03/18.
 */

public class App extends MultiDexApplication {

    private static App INSTANCE;

    public static synchronized App getInstance() {
        return INSTANCE;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        MultiDex.install(this);
        App.getInstance().readEnvironment();

        LocalStorage localStorage = new LocalStorage(this);
        if (!localStorage.isFound()){
            localStorage.init();
        }

        Configuration config = getBaseContext().getResources().getConfiguration();

        Locale locale = new Locale(localStorage.getLanguage());
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    /**
     * Read environment from setting.xml, depending on user build gradle profile
     * use to -Penv=dev||prod
     */
    public void readEnvironment() {
        ApiUtil.buildApi();
    }

    public String toJSON(Object o) {
        return new Gson().toJson(o);
    }

    public Object fromJSON(String json, Type t) {
        return new Gson().fromJson(json, t);
    }
}
